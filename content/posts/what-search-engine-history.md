---
 url : "what-search-engine-history"
 date : "2012-11-23"
 tags : 
  - "SEO" 
 title : "What is Search engine and the history of Search engine"
 description: "271979783817150464"
---
### What is search engine?
It’s a “computer function which searches through the internet to find data by using keywords or other terms, or a program containing this function” (Google, Search Engine Optimization starter guide, 2010).
A search engine works by fetching information from the websites, by sending out spiders and storing them in its giant database to help internet users in finding information. They are designed to retrieve appropriate information according to the user queries by using indexes which can read these data and make appropriate index for each document, to present them in their result pages in the forms of, images, web pages or even videos. Each search engine has its own algorithm to rank websites and boosting their results (Shi, 2006).

### History of Search engine.
Archie was the very first tool for searching the internet which was created at McGill University in 1990. It was programmed to download public files from website via File Transfer Protocol (FTP). It did so anonymously and create a centralise database of file names (Li, 2002).
Matthew Gray, in 1993, made the first index based search engine that utilised web-robots for the internet, using set of scripts which he had written by Perl language. It tried to determine the size of World Wide Web (Gray, 1993). In 1994, WebCrawler was the first full text crawler based search engine, defining a new standard in this market by letting users search the entire web content (Pinkerton, 2000), while Yahoo! helps people to find their interests via its directory instead of storing full-text copies of websites (Seymour, Frantsvog, & Kumar, 2011).
Many companies tried to enter this market speculatively and invested on the brightest search engine stars of 1990 era (Shi, 2006). Excite, Lycos, Magellan, Infoseek and yahoo were the five major search engines in said period (LaTimes, 1996) (CLARA, 1997).
In 2000 Google came up with innovative way of ranking pages, i.e. PageRank (Google, Technology overview, 2010), and thus acquiring better result in many search terms.

By 2003 Yahoo tried to compete with Google by acquiring search based services such as Inktomi (Kane , 2002) and Overtrue (Olsen & Kane , 2003). In 2004, finally, it switched to its own search engine (Seymour, Frantsvog, & Kumar, 2011).
At the same point Microsoft launched its own bot, i.e. msn bot in 2004, to start crawling the web pages, later rebranding its search engine and introduced Bing in association with yahoo in 2009, to become a Google competitor (Seymour, Frantsvog, & Kumar, 2011).

### References
- *Google. (2011). Search Engine Optimization (SEO). Retrieved January 7, 2012, from Google Webmaster Tools Help:[[1](http://www.google.com/support/webmasters/bin/answer.py?answer=35291)] *
- *Shi, Y. (2006). The search engine visibility of Queensland visitor information centres’ websites. iacis, 7(2), 228-232.*
- *Li, W. (2002). The first search engine, Archie. Retrieved January 7, 2012, from illinois: [[2]](http://people.lis.illinois.edu/~chip/projects/timeline/1990archie.htm)*
- *Gray, M. (1993). searchable index of the web. Retrieved January 7, 2012, from archive: [[3]](http://web.archive.org/web/20000919100351/http://ksi.cpsc.ucalgary.ca/archives/WWW-TALK/www-talk-1993q2.messages/706.html)*
- *Pinkerton, B. (2000). WebCrawler: finding what people want. Retrieved 01 07, 2012, from webir: [[4]](http://thinkpink.com/bp/Thesis/Thesis.pdf)*
- *Seymour, T., Frantsvog, D., & Kumar, S. (2011). History Of Search Engines. International Journal of Management & Information Systems, 15(4), 47-58.*
- *LaTimes. (1996). Browser Deals Push Netscape Stock Up 7.8%. Retrieved January 7, 2012, from Los Angeles Times: [[5]](http://articles.latimes.com/1996-04-01/business/fi-53780_1_netscape-home)*
- *CLARA, S. (1997). Yahoo! And Netscape Ink International Distribution Deal. Retrieved 01 07, 2012, from shareholder: [[6]](http://files.shareholder.com/downloads/YHOO/701084386x0x27155/9a3b5ed8-9e84-4cba-a1e5-77a3dc606566/YHOO_News_1997_7_8_General.pdf)*
- *Google. (2010). Technology overview. Retrieved January 7, 2012, from Google: [[7]](http://www.google.com/about/corporate/company/tech.html)*
- *Kent, P. (2008). Search Engine Optimization For Dummies (3rd ed.). Indianapolis: Wiley.*
- *Olsen, S., & Kane , M. (2003). Yahoo to buy Overture for $1.63 billion. Retrieved January 7, 2012, from Cnet: [[8]](http://news.cnet.com/2100-1030-1025394.html)*
- *Enge, E., Spencer, S., Fishkin, R., & Stricchiola, J. (2010). The art of SEO (1st ed.). (M. Treseler, Ed.) Sebastopol: O’Reilly.*
- *Jerkovic, J. (2010). SEO warrior (1st ed.). (M. Loukides, Ed.) Sebastopol: O’Reilly.