---
 url : "lean-kanban-continuous-delivery"
 date : "2012-09-19"
 tags : 
  - "Agile" 
  - "Methodology"
 title : "Lean startup + Kanban + Continuous delivery = success"
 description: "248470555794223104"
---

Yesterday I have attended [Quarterly briefing ThougthWorks](http://www.thoughtworks.com/events/lean-startup) event. different company attending the event and Laterooms was closest company to continuous delivery schema.

### what is Lean Start up focusing on ?

deliver product as soon as possible then add extra features (If something is not live it has no value).
adopting themselves with new customer needs by meeting them regularly. ([more info on wikipedia](http://en.wikipedia.org/wiki/Lean_Startup))

**examples of successful and unsuccessful start up**  

| startup | description |
|---------|-------------|
| {{< figure src="/media/instagram.jpg">}} | start as check in application change to photo sharing application launch their application in 8 week ([more info](http://articles.latimes.com/2012/apr/11/business/la-fi-instagram-systrom-20120411)) |
| {{< figure src="/media/boo-com.jpg">}} | spend 135£ million on implementing it in just 18 month their website was hard to use founded in 1998 and launch in 2000 (more info in [wikipedia](http://en.wikipedia.org/wiki/Boo.com) and [Cnet](http://crave.cnet.co.uk/gadgets/the-greatest-defunct-web-sites-and-dotcom-disasters-49296926/3/)) |

### What is Kanban?

Kanban is approach that can help your current deployment, to have more flow. It helps us to reveal bottle necks ([more info](http://www.kanbanblog.com/explained/index.html)).
{{< figure src="/media/waterfall1.jpg">}}  
{{< figure src="/media/scrum.jpg">}}  

> in above picture,  development is a bottle neck and team should not push more than 5 ticket into it

### what should we focus on using Kanban?

- Visualizing the work that is in progress
- unblocking the tickets
- learning from mistake to reduce the cycle time
### Kanban VS waterfall VS Scrum 

> if you afraid of release , do it more to get more confidence on it !

### What is continuous delivery ?

its the process to help us to improve the process of software delivery. using various techniques such as automated testing , continuous deployment and continuous integration help us to deliver values faster and more reliable with less bugs ([more info](http://en.wikipedia.org/wiki/Continuous_delivery)) .

{{< figure src="/media/continuous_delivery_process_diagram.jpg">}}  

### Q&A

**Q**: How to get a feedback from customer , if we making an enterprise application ?  
**A**: you should not use, survey necessary to get the feedback. you can use Google analytic or other analyzing tools or even bring customer to your company to know his ideas about the application  .

**Q**: How to use Kanban correctly if our tasks are tie together and can not go to live without each other ?  
**A**: Kanban  aim to increase the flow , in this case you can define the larger task.

**Q**: What happen if we find the live defect and already touch the WIP limit?  
**A**: usually you can write number on the limit sign, that shows how many urgent ticket can add to a column if it reach to WIP limit . urgent ticket could define by business as a live defect or any other thing .

**Q**: What is the different between continuous delivery and continuous deployment ?  
**A**: Deployment is not necessary a release. Continuous deployment implying to bring every successful  build which pass automated tests to users. while continuous delivery focusing on bringing the release to the hand of business and let them to touch the release button when they want them ([more info](http://continuousdelivery.com/2010/08/continuous-delivery-vs-continuous-deployment/)).