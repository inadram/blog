---
 url : "google-unofficial-factors-in-seo"
 date : "2013-01-11"
 tags : 
  - "SEO"
 title : "Google Unofficial factors in SEO"
 description: "289784017547583490"
---
Unofficial ranking factors are always argued between SEO experts. Some of them are totally rejected by search engine as a cheating, like “link farming” (Google, Link schemes, 2011), “clock threading” (Google, Cloaking, sneaky Javascript redirects, and dorway pages, 2011), “hidden text” (Google, Hidden text and links, 2011) and “automated queries” (Google, Automated queries, 2011) but still there are plenty of methods that never officially accepted or rejected by Google.  

This section of the article tries to shed light on different ideas whole the some of them such as “Best title”, “Keyword density” and “Nofollow links” .

### Age of the domain
Jerkovic claims that search engine trust on the pages which comes from older domains (Jerkovic, 2010) at the same point Enge et al believe that as the website with poor quality or spammy material, could not last long therefore search engines get more credits to the content of the old website (Enge, Spencer, Fishkin, & Stricchiola, 2010). Grappone proposed the same idea and noted that webmasters have noticed that it’s harder to get good ranks with newer domain as compared to the older ones (Grappone & Couzin, 2011) and even Baylin suggests that webmasters buy old domains (Bailyn & Bailyn, 2011).  

On the other hand, Cutts a team member of Google said that, webmasters should not worry about these issues (GoogleWebmasterHelp, How much does a domain’s age affect its ranking?, 2009).but he did not clarify that if Google’s concern and state of opinion about domain age in sorting result or not. In the same vein, Ledford, Fox and Michael did not take it into account as a factor at all (Ledford, 2009) (Fox, 2010) (Michael & Salter, 2008).

### Best title
“Do keep it short” says (Grappone & Couzin, 2011, p. 173). Most of the search engines show only up to 60 characters of the titles in their search result therefore webmasters should keep their titles short (Jerkovic, 2010, p. 64) (Grappone & Couzin, 2011, p. 173) (Michael & Salter, 2008, p. 60) (Bailyn & Bailyn, 2011, p. 29). In addition Couzin and Grappon strongly recommended avoiding repeating keywords in titles (Grappone & Couzin, 2011, p. 173). Similarly Peter Kent in the third edition of the “Search Engine Optimization for Dummies” believes in short titles but he recommended the repetition of the most important keyword of the page only for one time (Kent, 2008, p. 35). On the other hand Konia in the manual of the “Web Position Gold” which is a famous black hat SEO tool recommend to the webmasters to use their primary keywords in the title tag at least once. He said webmasters can attract more traffic by using the same keyword in the title multiple times but in different rows. He also stood against the “Short title” idea and asks webmasters to use longer titles to achieve better position in search results. Konia supports his theory by claiming that it is easier to persuade the searchers to visit a website by longer titles (Konia, 2002, p. 133); at the same point, Enge et al advocate for the long titles and said “Target longer phrases if they are relevant”. Enge et al and Fox believed that are more accurate and descriptive titles being more beneficial in comparison with simple titles which are ambiguous and reduce the readability (Enge, Spencer, Fishkin, & Stricchiola, 2010, p. 212) (Fox, 2010, p. 147). Besides these, Jerkovic suggested to use relevant titles for each page that contain keywords of the page (Jerkovic, 2010, p. 65).  

However Google tries to step out and just recommend having brief but descriptive titles which could be interpreted with all the above arguments (Google, Search Engine Optimization starter guide, 2010).

### Keyword density
Keyword density or in other words the number of times that a specific keyword is repeated in the content, is one of the most important factors that almost all SEO experts believe in. However, there is different point of views about the best keyword density percentage for generating better results.  

Jerkovic believes that a good keyword density should stand between 0.2% and 4%. At the same point he claims that if you go beyond 10%, search engines will penalise you (Jerkovic, 2010, p. 67). Also, the vendor of the “Web Position Gold” says that this percentage could vary from 1% to 4% according to your targeted search engine (Konia, 2002, p. 19). On the other hand, Kent does not believe that there is a certain percentage of all the cases (Kent, 2008, p. 105). Similarly, Baylin totally ignores the role of keyword density in search engines (Bailyn & Bailyn, 2011, p. 135). Also, Enge et al. Believes that search engines use more sophisticated analyses than counting keywords of the page which will not be useful in many cases at all (Enge, Spencer, Fishkin, & Stricchiola, 2010, p. 158). All In all, although Google does not encourage webmasters to repeat their keywords within the content of their websites, it never denied the role of “keyword density” in SERP.

### Nofollow Links
Nofollow was the one of the first attributes that widely accepted on the Internet (Cover, 2011, p. 178). Google introduces “Nofollow” attribute to combat with spamming (Google, Search Engine Optimization starter guide, 2010). Webmasters should try to attain links which do not have “nofollow” attribute on them and sometimes called “dofollow” (Grappone & Couzin, 2011, p. 230). Webmasters can add “rel=nofollow” attribute to the certain links that search engines should not follow or pass the page’s reputation to them. (Google, Search Engine Optimization starter guide, 2010), (Grappone & Couzin, 2011, p. 85), (Ledford, 2009, p. 215) , (Jones, 2008, p. 70).

|{{<figure src="/media/nofollow.jpg">}}|
|---|
|Figure : “Nofollow” attribute in HTML (Google, 2010)|

It’s really useful for the websites with public commenting features that wishes not to pass their reputation to the spammer robots or even sleazy webmasters that that leave comments on their pages only for this purpose. Also this attribute can also append to the links that are not directly related to the content of the page (Jones, 2008, p. 70). Besides these, Jerkovic believes that some search engines like Ask.com ignore this attribute and follow the links (Jerkovic, 2010, p. 371). Similarly Enge et al. Thinks that due to the lack of semantic logic, search engine’s robots do not care about this attribute and they get crawled (Enge, Spencer, Fishkin, & Stricchiola, 2010, p. 247). Along with these points, Odom and Viney believe that search engines follow the “nofollow” links but they won’t pass any link juice and they are not counted in Google page rank calculation algorithm (Viney, p. 227) (Odom, 2010, p. 45).

|{{<figure src="/media/nofollow-arc.jpg">}}|
|---|
|Figure : How Googlebot treats with nofollow (Enge et all, 2010)|

Having a nofollow links in comment section does not necessarily mean that those links are worthless and your readers should ignore them (Odom, 2010). In addition Ledford thinks that nofollow links not only can prevent a website to lose its page rank but also it can increase its ranking (Ledford, 2009, p. 215). However, Cutts who is at Google’s team member ,in 2009 claim that although Google changed its policy for “ nofollow “ links, still ignore them as a valuable link (Cutts, PageRank sculpting, 2009). All in all, it seems that Google still crawls them and this might effect on their position in SERP too.

### Keyword prominence
Keyword prominence is referring to the relative keyword location in a webpage. Some SEO expert believes that Google pays more attention on the words which appear on the top of the page (Kent, 2008, p. 104) (Jerkovic, 2010, p. 71) (Murray, 2009, p. 120) especially if the page is very large (Jerkovic, 2010, p. 71). Jerkovic believes that although keywords should appear throughout the page, putting important keyword on the top section of the page is better (Jerkovic, 2010). In the same way Odom stated that keyword prominence play a more important role when these keywords have higher density in your content (Odom, 2010). Similarly Ledford said, keyword prominence become important when these keywords appear in Meta tags, headline, and first paragraph. (Ledford, 2009, p. 408)

### Outbound link
Outbound link has been discussed before in Google official factor section. This subject has often been debated. There are webmasters that worry about making outbound link. As they think it might cause to lose their PageRank and also their visitors when they sending them out of their website. While on the other hand there are some who believe that having an only inbound link with no outbound link cannot look good and the best plan is to have a balance between them (Ledford, 2009, p. 268). Linking to another site might seem as an ill-advised in first look but it can help visitors to find relative sources and search engines will find out that you are adding value to web (Murray, 2009, p. 43) .In other words it is another way to improve a website ranking by using its keyword to well-known websites (Bailyn & Bailyn, 2011, p. 160). Enge et al. Believe that having a low quality outbound link to the spammy websites could hurt a website reputation and its ranking (Enge, Spencer, Fishkin, & Stricchiola, 2010, p. 52). Having outbound links that engage in linking schemes can negative effect on a website ranking. Therefore it’s important to remove the questionable links, especially when there is some old content that might link to the websites which are now gone over dark side (Grappone & Couzin, 2011, p. 211) (Ledford, 2009, p. 269). Similarly Peter Kent said that having good outbound link can help a website rank better in search engine result pages (Kent, 2008, p. 430) while Jerkovic stated that having an outbound link reduce a website popularity regardless of the quality of the links (Jerkovic, 2010, p. 92).

### References
- *Bailyn, E., & Bailyn, B. (2011). Outsmarting Google (1st ed.). (S. Schroeder, G. Wiegand, R. Kughen, T. Simpson, K. Cline, & S. Baldwin, Eds.) Indianapolis: Que.*
- *Cover, D. (2011). Search Engin Optimization Secrets. (C. Long, Ed.) Indianapolis: wiley.*
- *Cutts, M. (2009). PageRank sculpting. Retrieved January 7, 2012, from mattcutts: [[link]](http://www.mattcutts.com/blog/pagerank-sculpting/)*
- *Enge, E., Spencer, S., Fishkin, R., & Stricchiola, J. (2010). The art of SEO (1st ed.). (M. Treseler, Ed.) Sebastopol: O’Reilly.*
- *Fox, V. (2010). Marketing in the Age of Google: Your Online Strategy IS Your Business Strategy. New Jersey: John wiley & Sons.*
- *Google. (2010). Technology overview. Retrieved January 7, 2012, from Google: [[link]](http://www.google.com/about/corporate/company/tech.html)*
- *Google. (2011). Automated queries. Retrieved January 8, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=66357)*
- *Google. (2011). Cloaking, sneaky Javascript redirects, and doorway pages. Retrieved January 8, 2012, from Google Webmaster Tools Help: [[link]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=66355)*
- *Google. (2011). Hidden text and links. Retrieved January 8, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=66353)*
- *Google. (2011). Link schemes. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=66356)*
- *Google. (2011). Search Engine Optimization (SEO). Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=35291)*
- *GoogleWebmasterHelp. (2009, May 7). How much does a domain’s age affect its ranking? Retrieved January 14, 2012, from youtube: [[link]](http://www.youtube.com/watch?v=Y1_1NQWQJ2Q)*
- *Grappone, j., & Couzin, G. (2011). Search engine optimization An hour a day (3rd ed.). (P. Gaughan, Ed.) Indianapolis: Willey.*
- *Jerkovic, J. (2010). SEO warrior (1st ed.). (M. Loukides, Ed.) Sebastopol: O’Reilly.*
- *Jones, K. (2008). Search Engine Optimization: Your Visual Blueprint for Effective Internet Marketing. (R. Siesky, Ed.) Indianapolis: Wiley.*
- *Kane , M. (2002). Yahoo to acquire Inktomi. Retrieved January 7, 2012, from Cnet: [[link]](http://news.cnet.com/2100-1023-978692.html)*
- *Konia, B. (2002). Search Engine Optimization with WebPosition Gold (2nd ed.). Texas: Wordware.*
- *Ledford, J. (2009). Search Engine Optimization (2nd ed.). (M. B. Wakefield, Ed.) Indianapolis: Wiley.*
- *Michael , A., & Salter, B. (2008). Marketing Through Search Optimization (2nd ed.). Oxford: Elsevier Ltd*
- *Murray, G. (2009). Seo Secrets (2nd ed.). Divine Write.*
- *Odom, S. (2010). SEO for 2010 Search Engine Optimization Secrets. MediaWorks.*
- *Viney, D. (2008). Get to the Top on Google. London: Nicholas Brealey.*