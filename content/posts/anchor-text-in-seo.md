---
 url : "anchor-text-in-seo"
 date : "2013-01-04"
 tags : 
  - "SEO" 
 title : "Anchor text in SEO"
 description: "287180934791430146"
---

Anchor text is a clickable text that a user sees on a link (Google, Search Engine Optimization starter guide, 2010). The anchor text helps a website to describe its content to search engines and visitors. The number of relevant links to a webpage will show its usability and popularity (Google, BlogHer 2007: Building your audience, 2007) (Google, Search Engine Optimization starter guide, 2010).

|{{< figure src="/media/12.jpg" >}}|
|---|
|Figure : Internal links help GoogleBots (randfish, 2007)|

Short but descriptive anchor text makes it easier for both Google and users to understand the content of the page while choosing off-topic anchor text which has no relation to the content of target page and is of no use.  

Google asked webmaster to make it easy for visitors to distinguish between anchor text links and regular text, and avoid using lengthy anchor text that uses keywords excessively for search engines. (Google, Search Engine Optimization starter guide, 2010).  

In addition, recently Google enhanced its algorithm against the duplicate boilerplate anchor links and put less weight on these types of links as Google find them less relevant (Cutts, Ten recent algorithm changes, 2011).

### References
- *Google. (2007). BlogHer 2007: Building your audience. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2007/03/blogher-2007-building-your-audience.html)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Cutts, M. (2011). Ten recent algorithm changes. Retrieved January 7, 2012, from inside search, The official Google search blog: [[link]](http://insidesearch.blogspot.com/2011/11/ten-recent-algorithm-changes.html)*
- *randfish. (2007, September 2007). My Personal Opinion – 90% of the Rankings Equation Lies in These 4 Factors. Retrieved January 14, 2012, from seomoz: [[link]](http://www.seomoz.org/blog/my-personal-opinion-90-of-the-rankings-equation-lies-in-these-4-factors)*
- *Google. (2010). Technology overview. Retrieved January 7, 2012, from Google: [[link]](http://www.google.com/about/corporate/company/tech.html)*