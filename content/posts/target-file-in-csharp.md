---
 url : "target-file-in-csharp"
 date : "2012-11-12"
 tags : 
  - "ASP.NET" 
  - "Programming"
 title : "Example of using Target file in C# project"
 description: "245930673779654657"
---
### What is target file ?
projects use .target files to define which files should be imported into the project during the build process ([more info](http://msdn.microsoft.com/en-us/library/ms164312.aspx)).
{{< figure src="/media/target.jpg">}}

### How to use it ?

1.in order to use target files firstly you need to tell your Visual C# project to use this future by adding below line to the project file (.csproj).

```html
<Import Project="$(MSBuildToolsPath)\Microsoft.CSharp.targets" />
```

2.Then you need to import the targetFile address as below

> my target file name is “TargetFileApp.csproj.targets“

3.In the next step you will ask the compiler when to do the target files task

```
<Target Name="AfterBuild">
  <CallTarget Targets="CopyPackageContent" />
</Target>
```

> A target never build twice but you can order the build by using  BeforeTargets and AfterTargets ([read more](http://msdn.microsoft.com/en-us/library/ee216359.aspx)) .
 
> by above line it goes to my target file and looking for “CopyPackageContent” element.

4.now you can add the your desirable tasks to your target file

```html
<?xml version="1.0" encoding="utf-8"?>
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <ItemGroup>
   <TestBase Include="Test\*.*" Exclude="Test\*.etc" />
  </ItemGroup>
  <Target Name="CopyPackageContent">
  <Copy SourceFiles="@(TestBase)" DestinationFiles="@(TestBase-&gt;'$(OutDir)%(RecursiveDir)%(Filename)%(Extension)')" />
    <Warning Text="Content copied from package TestBase.Base to the output folder. Cleaning the project/solution will not delete these files." />
    </Target>
</Project>
```
> In the ItemsGroup you can define your addresses and filter them with Exclude attribute . for instance in this case, it is looking for all the files which exist in Test folder except the one has .etc extension.

when the project is build the compiler find the “CopypackageContent” element in the target file and then start copying from pre defined addresses to destination address ([more info about copy task](http://msdn.microsoft.com/en-us/library/3e54c37h.aspx)) .

download the whole source of TargetFileApp from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/ASP.NET/targetFileGenerator).

added : I also make a simple [application to generate the target file for CSharp project](http://blog.inadram.com/target-file-generator-for-csharp-application-source-code/).