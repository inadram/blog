---
 url : "csharp-example-rhino-automocker-structuremap"
 date : "2013-01-13"
 tags : 
  - "Programming"
  - "Structuremap"
  - "Test"
 title : "Csharp example of using Rhino AutoMocker Structuremap"
 description: "290562211229872128"
---
### What is AtuoMocker?
Structuremap Automocker is an IOC container that help us to construct classes with all of its dependencies.you can download Structuremap via [Nuget](http://nuget.org/packages/structuremap)  . [[more info](http://docs.structuremap.net/)]

### When using it ?
Sometimes your class has too many dependencies and you don’t want to worry about them. Also it is useful when your classes’s constructor change frequently and you don’t want to break your tests.

#### Example
In the code below we have Machine class which have two dependencies i.e. “IDeviceManager”, “Iproduct”.

```
public class Machine {
	private IDeviceManager _deviceManager;
	private Iproduct _product;
	public Machine(IDeviceManager deviceManager,Iproduct product) {
	    _deviceManager = deviceManager;
	    _product = product;
	}

	public void RegisterNewDevice(Device device) {
		_deviceManager.AddedSucessfully = true;
		if (!_product.IsOutOfStock(device)) {
		    _deviceManager.Add(device);
		}
		_deviceManager.special = _deviceManager.IsSpecialProduct(device) ? "Special" : "Not Special";
	}
}
```    
#### Mock
In the test section, we try to assert that if “AddedSucessfully” set to true or not . In this case we can use RhinoAutoMocker to Mock the concrete Machine Class without worrying about its dependencies.

```
[Test]
public void Given_device_when_RegisterNewDevice_then_AddedSucessfullyShouldSetToTrue() {
	var autoMocker=new RhinoAutoMocker<Machine>();
	var device=new Device {ID=1,Name = "printer"};
	autoMocker.ClassUnderTest.RegisterNewDevice(device);
	autoMocker.Get<IDeviceManager>().AssertWasCalled(x => x.AddedSucessfully = true);
}
```        
#### Stub

In order to test that if the Add method will be called if the IsOutOfStock return false . we have to stubbing the interface of product class like below.

```
[Test]
public void Given_device_when_TheStockIsNotOutOfOrder_then_AddMethodShouldCalled() {
	var autoMocker = new RhinoAutoMocker<Machine>();
	var device = new Device { ID = 1, Name = "printer" };
	var product = autoMocker.Get<Iproduct>();
	product.Stub(x => x.IsOutOfStock(Arg<Device>.Is.Anything)).Return(false);
	autoMocker.ClassUnderTest.RegisterNewDevice(device);
	autoMocker.Get<IDeviceManager>().AssertWasCalled(x => x.Add(device));
}
```  
      
### Download
Feel free to download the source code of this example from my [ [GitHub](https://github.com/inadram/TestingApproches/tree/master/Auto_Mocker/Auto_Mocker) ].