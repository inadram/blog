---
 url : "scala-basic-syntax"
 date : "2014-10-11"
 tags : 
  - "Programming"
  - "scala"
 title : "Scala basic syntax by example"
---

All data types are objects and there are no primitives and you can call methods on an Int or a Long.

### Scala Data Types

```
Boolean
Byte, Short, Int, Long
Float, Double
Char, String
Unit(the 'void' type)
```
### Scala Variables
Compiler can infer type, so if you does not assign type to variable it will automatically set it according to the value.

```
val n = 5 // type is int
var f = 3.0 // type is Double

val someInt:String = "test String"
val SomeChar: Char ='a'
```

### Scala Operators
Similar to java but there is no ++ or — assignments.

```
var someInt =10
someInt+=5 //15
someInt-=4 //11
```
Scala Parentheses and Curly Braces
Parentheses use for function arguments.

### Curly braces represent a block

```
def someFunc = {(x:Int)=> x*x}
someFunc(2)
```
the output for it will be `4`

### Scala Conditions
you can use if and pattern matching.

`‘if’` is an expression and it returns value to status

```
def checkCondition(someCondition: Int) = {
  val status = if (someCondition == 1) {
    1
  } else if (someCondition == 2) {
    2
  } else {
    3
  }
}
```
the result type of “else if” block in Unit.

### Scala Loops
there is no ‘break’ or ‘continue’ in for loop.

in the below example it loop through string, char by char

```
def forLoop(someValue:String) = {
  for(c<- someValue){
    println(c)
  }
}
 
forLoop("amir mardani")
```
> output will be look likes below

```
a
m
i
r
m
a
r
d
a
n
i
```
#### Counted Loops

```
def countedLoop(someNumber:Int)={
 
  for(i <- 1 to someNumber){
    println(i)
  }
 
  for(i <- 1 until someNumber){
    println(i)
  }
}
 
countedLoop(4)
```
> first loop output

```
1
2
3
4
```
> second loop output

```
1
2
3
```
#### Multiple nested loop

```
def multipleNestedLoop(someNumber:Int)={
  for(i <- 1 to someNumber;j <- 1 until someNumber){
    println("i= " +i + " , j= "+ j)
  }
}
multipleNestedLoop(4)
```
output will be look like below

> output

```
i= 1 , j= 1
i= 1 , j= 2
i= 1 , j= 3
i= 2 , j= 1
i= 2 , j= 2
i= 2 , j= 3
i= 3 , j= 1
i= 3 , j= 2
i= 3 , j= 3
i= 4 , j= 1
i= 4 , j= 2
i= 4 , j= 3
```  
#### Guards
you can add guards or filter the loop generator

```
def multipleNestedLoopWithGuard(someNumber:Int)={
  for(i <- 1 to someNumber ;j <- 1 until someNumber if i!=j if i!=1){
    println("i= " +i + " , j= "+ j)
  }
}
multipleNestedLoopWithGuard(4)
```
The output will be filter the values where “i” equals to “j” and also “i” is equal to “1″

```  
i= 2 , j= 1
i= 2 , j= 3
i= 3 , j= 1
i= 3 , j= 2
i= 4 , j= 1
i= 4 , j= 2
i= 4 , j= 3
```
if the body of loop is a ‘yeild’ statement then Scala will be constructs a collection of values .

```
def yieldLoop(someInt:Int)={
  for(i<-1 to someInt) yield i*2
}
yieldLoop(5)
```
The output will be a vector collection

`scala.collection.immutable.IndexedSeq[Int] = Vector(2, 4, 6, 8, 10)`
### Scala Functions
methods are belongs to Classes while functions can be belong to Class, object or even Repel.

it define with “def” and the name can almost be anything. The body of function come after “=”.

You don’t have to define return type except for recursive functions.

The last expression is evaluated as return value.

```
def someValue ={
  println("hello world")
}
someValue
```

if nothing to return, the type is ‘Unit’ or you can omit the ‘=’

```
def someValueWithUnit:Unit ={
  var someVal = "hello world"
  someVal
}
someValueWithUnit
```

it wont return any value in both cases

```
def someValueWithoutEqualSign{
  var someVal = "hello world"
  someVal
}
someValueWithoutEqualSign
```

if there is no argument then you don’t have to use ()

#### Set argument position for methods

```
def someValueWithPosition(first: String, second : String) ={
  println(first+second)
}
someValueWithPosition(second =" 2nd value ",first =" 1st value ")
```

the values will be pass to the named arguments and the output will be look like below

`1st value  2nd value`
### Scala Simple I/O
you can use “print” and “println” like java and also use “readLine”, “readInt” and “readBoolean” to get user input.

`def userInput ={ var someValue = readLine("Enter user value"); println("the value is : "+ someValue)}
`
`def userInput ={ var someValue = readInt(); println("the value is : "+ someValue)}
`to print variable values use “s” keyword

```
val name = "amir"
println(s"my name is $name")
```
the output will be

```
my name is amir
```
to print variable values with format use “f” keyword

```
val height = 100.7652
println(f"my height is $height%2.2f cm")
```
the output will be

```
my height is 100.77 cm
```
to print raw string use “raw” keyword

```
println(raw"amir \nb")
the output will be
```

```
amir \nb
```
