---
 url : "target-file-generator-for-csharp-application-source-code"
 date : "2012-10-04"
 tags : 
  - "ASP.NET" 
  - "Programming"
 title : "Target File generator for CSharp application + source code"
 description: "253884626173566976"
---
I just made a simple application with Nunit and c# to generate target file content for any CSharp projects ([download it from my github](https://github.com/inadram/ASP.NET.git)).

### what is TargetFile ?

it help you to move your resources in the projects([more info about target files](http://blog.inadram.com/target-file-in-csharp/)).

### How to use the application ?

Short answer :just run the app , you will find out !

Long answer :After you download and run the “exe” file which located in “bin” folder, the program ask you to choose the project file.

{{< figure src="/media/targetfilegenerator.jpg">}}  

in the next page “Browse” the source file and if you need to exclude any file say “No” to the next dialogue .

{{< figure src="/media/findsourcefile.jpg">}}  

now all the power in you hand and you can nicely , tidy up the list of the files that you can remove by simply adding rules to the list and see the remaining files immediately , or even remove any unwanted rules.

{{< figure src="/media/filtersourcefile.jpg">}}  

and finally after adding all desirable rules , target file will be generated by clicking on the “Finish” button !

All left is just edit the .csproj files to ask it use the target file ([how to do it ?](http://blog.inadram.com/target-file-in-csharp/)) .