---
 url : "importance-of-site-speed-in-seo"
 date : "2012-12-10"
 tags : 
  - "SEO" 
 title : "Importance of Site Speed in SEO"
 description: "278250791352008704"
---
“Site speed shows how quickly a website response to web requests”. Google did some experiment to understand the importance of websites speed on its user satisfaction (Brutlag, 2009).
Google found out that faster websites not only reduce the operating costs but also improve user experience as they love faster sites while users spend less time in slower websites. Although they include this signal in their search ranking algorithm to encourage webmasters to compact their website with site speed standards, it does not carry too much weight (GoogleWebmasterHelp, Is speed more important than relevance?, 2010).
Currently, less than 1% of search queries are affected by this factor and at this point it is restricted to the search queries which perform on “Google.com” (Singhal & Cutts, Using site speed in web search ranking, 2010).

### References
- *Brutlag, J. (2009). Speed Matters. Retrieved January 7, 2012, from Research Blog: [[link]](http://googleresearch.blogspot.com/2009/06/speed-matters.html)*
- *GoogleWebmasterHelp. (2010). Is speed more important than relevance? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=muSIzHurn4U)*
- *Singhal, A., & Cutts, M. (2010). Using site speed in web search ranking. Retrieved January 7, 2012, from google webmaster central: [[link]](http://googlewebmastercentral.blogspot.com/2010/04/using-site-speed-in-web-search-ranking.html)*