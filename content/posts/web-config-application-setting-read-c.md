---
 url : "web-config-application-setting-read-c"
 date : "2012-11-04"
 tags : 
  - "ASP.NET" 
  - "Programming" 
 title : "How to setup application setting in web.config & read them c#"
 description: "264888737132052480"
---
### Why use web.config ?
when your application have some general configs that you want to be able to change them easily without dealing with code compiling, putting them in the web.config file is a wise choice . 
   
{{< figure src="/media/1-300x114.jpg">}}

### How to put setting in web.config ?
#### Make sure you open the right web.config file . 
Choose `Web.Debug.config` if you want your setting be only available in Debug state.

Choose `Web.Release.config` if you want your setting be only available in Release state.

Choose `Web.config` for both states.

#### put them in the right section
put your key value pair in `<appSetting>` node like below.

```
<appSettings>
		<add key="ReturnUrl" value="/Account/ExternalLoginCallback?ReturnUrl=%2fAnalyseTwitter" />
</appSettings>
```   
### How to read application setting from web.config

Using `System.Web.Configuration` name space in the file that you wish to have your application setting and read they key with below syntax.

```
string UrlValue=WebConfigurationManager.AppSettings["ReturnUrl"];
```
 