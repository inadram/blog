---
 url : "csharp-example-singleton-pattern"
 date : "2013-02-05"
 tags : 
  - "Programming"
  - "Design pattern"
 title : "Csharp example of Singleton design pattern"
 description: "298590962530275328"
---

### What is Singleton design pattern?
The idea of singleton pattern is to have only one instance of a class or in other words have one instance of class when only need it . The class is responsible to make only one instance of itself.

We can use this pattern when the class does not have parameter  in its constructor and making objects of the class is expensive.

### Example  

#### What We have?
We have some class that gets the creation time when its initialize through its constructor . It’s set this value in the  CreationTime property.

##### main class

```
public class SingeltonObject {
        private string _thecreationTime;
        public SingeltonObject() {
            _thecreationTime = DateTime.Now.ToString();
        }

        public string CreationTime { get { return _thecreationTime; } }
}
```

We want to make sure that every time that we call this class it do not make a new object and still return the first creation time value.

##### calling main class

```
public class Account {
        public string GetCreationTime() {
            SingeltonObject singeltonObject=new SingeltonObject();
            string creationTime = singeltonObject.CreationTime;
            return creationTime;
        }
}
```

##### Test
Currently it won’t return the same instance and we can prove it by writing some test around it.

> Tests  

```
[TestFixture]
public class TestSingelton {
        [Test]
        public void CallSingeltonObject_ShouldReturnOneInstance() {
            var Account = new Account();
            string creationTime = Account.GetCreationTime();
            Thread.Sleep(1000);
            string creationTime2 = Account.GetCreationTime();
            Assert.That(creationTime, Is.EqualTo(creationTime2));
        } 
}
```

### How To apply Singelton pattern
1. Make constructor private

We should change the access modifier of the constructor to private to do not let it to call externally.

> constructor

```
private SingeltonObject() {
	_thecreationTime = DateTime.Now.ToString();
}
```
2. Make static variable

Then make a static variable of the current class type.

> static variable

```
private static SingeltonObject _instance;
```


3. Make static property

We are making static property that checks if is there any instance of our singleton class or not . It helps us to make sure that we only have one instance of our class in our application.

> static property

```
public static SingeltonObject Instance {
            get {  if (_instance == null) {
                    _instance = new SingeltonObject();
                    }
                return _instance;
            }
}
```

Now if we call our class, it first checks that if there is any instance of it in the application and then make new instance

> implementation

```
public string GetCreationTime() {
	return SingeltonObject.Instance.CreationTime;         
}
```

### Problems
The default construction of singleton pattern is not thread safe , so it is not suggested for web application.
Singleton patterns are hard to test and we label them as an anti – pattern
Its violate Single responsibility principle because it has different behavior in its lifetime. Because it also checks that it is there any instance of the class or not along with its responsibilities. We can use an IOC container to check the lifetime of the class and make it to have SRP.

### Download
Feel free to download the full source code of this singleton example from my [GitHub](https://github.com/inadram/Design-Pattern/tree/master/SingeltonPattern).

