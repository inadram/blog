---
 url : "csharp-example-rhinomock-stub"
 date : "2013-01-13"
 tags : 
  - "Programming"
  - "RhinoMock"
  - "Test"
 title : "CSharp example of mocking and stubbing with RhinoMock"
 description: "290263426826125312"
---
### What is RhinoMock?
RhinoMock is a mocking framework that helps you to inject dependencies without implementing them in your test side.

When you are mocking an interface, you are making an object in a proxy domain that can at as a real class for you.[more info](http://ayende.com/wiki/Rhino+Mocks+Documentation.ashx?AspxAutoDetectCookieSupport=1)

### How to use it ?

Imagine that you have below code ,and you want to assert that if the Add method is called or not .

```
public class Machine {
		public void RegisterNewDevice(Device device) {
		    var deviceManager=new DeviceManager();
		    deviceManager.Add(device);
		    deviceManager.AddedSucessfully = true;
		    deviceManager.special = deviceManager.IsSpecialProduct(device) ? "Special" : "Not Special";
		}
}
```

The class is simply violating SOLID principal as you can not inject DeviceManager into your RegisterNewDevice method . Therefore you have to extract the interface of the class and initialize it in the constructor of the Machine class.

```
public class Machine {
		private IDeviceManager _deviceManager;
		public Machine(IDeviceManager deviceManager) {
		    _deviceManager = deviceManager;
		}
		public void RegisterNewDevice(Device device) {
		    _deviceManager.Add(device);
		    _deviceManager.AddedSucessfully = true;
		    _deviceManager.special = _deviceManager.IsSpecialProduct(device) ? "Special" : "Not Special";
		}
}
```

There you go , now it is ready to test it with Rhino Mock !

> Bring dependencies out into constructor

```
class Machine {
		private IDeviceManager _deviceManager;
		public Machine(IDeviceManager deviceManager) {
		    _deviceManager = deviceManager;

		}
		public void RegisterNewDevice(Device device) {
		    _deviceManager.Add(device);
		}
}
```

The next step is mocking the the dependency and passing it into the Machine class in the arrang section. Then you call your suppose method and assert if the add method is called or not .

> Assert Method Was CalledC#

```
[Test]
public void Given_Device_When_RegisterNewDevice_then_AddShouldCalled() {
		//Arange
		var machine = new Machine(deviceManager);
		var device=new Device {Id = 1, Name = "printer"};
		     var deviceManager = MockRepository.GenerateMock<IDeviceManager>();
		//Act
		machine.RegisterNewDevice(device);
		
		//Assert
		deviceManager.AssertWasCalled(x=>x.Add(device));
}

```

Also you can come with similar code to assert that if the AddSuccessfully property is set to true or not .

> Assert PropertyC#

```
[Test]
public void Given_Device_When_RegisterNewDevice_then_AddedSucessfullyShouldSettoTrue() {
		//Arange
		var deviceManager = MockRepository.GenerateMock<IDeviceManager>();
		var machine = new Machine(deviceManager);
		var device = new Device { Id = 1, Name = "printer" };

		//Act
		machine.RegisterNewDevice(device);

		//Assert
		deviceManager.AssertWasCalled(x => x.AddedSucessfully=true);
}
```
Sometimes you might need to assume that a method return your suppose value to write your test . For instance, to test that if the “Special” value set to its property if the method `IsSpecialProduct()` return true , you have to stubbing that method.

> Stub ValueC#

```
[Test]
public void Given_Device_When_IfSpecialProduct_then_SpecialShouldSet() {
		//Arange
		var deviceManager = MockRepository.GenerateMock<IDeviceManager>();
		deviceManager.Stub(x => x.IsSpecialProduct(Arg<Device>.Is.Anything)).Return(true);
		var machine = new Machine(deviceManager);
		var device = new Device { Id = 1, Name = "printer" };

		//Act
		machine.RegisterNewDevice(device);

		//Assert
		eviceManager.AssertWasCalled(x => x.special="Special");
 }
```
The code says that regardless of input value of the `IsSpecialProduct()` always return true. Then we can assert that in that case the suppose value is set to the property or not

### Download
Feel free to download the source code of this example from my [GitHub](https://github.com/inadram/TestingApproches/tree/master/RhinoMock/Rhino_Mock)
