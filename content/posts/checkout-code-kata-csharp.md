---
 url : "checkout-code-kata-csharp"
 date : "2013-03-17"
 tags : 
  - "C#"
  - "checkOut"
  - "CodeKata"
 title : "CheckOut codekata with cSharp"
 description: "313434055800655872"
---

### What is CheckOut codeKata ?
The goal in Check out `codekata` is to find the way to calculate total price of items. there is some discount for some of the items that cause this codeKata become a little more challenging.  

|Item|Unit Price|Special Price|
|----|----------|-------------|
|A   |50        |3 for 130    |
|B   |30        |2 for 45     |
|C   |20        |             |
|1   |          |             |
|2   |          |             |
|3   |          |             |
|4   |          |             |
|5   |          |             |
  
  
|Item|Unit Price|Special Price|
|----|----------|-------------|
|A   |50        | 3 for 130   |
|B   |30        | 2 for 45    |
|C   |20        |             |

  
You can find more info about the scenario [here](http://codekata.pragprog.com/2007/01/kata_nine_back_.html).

### Download
Feel free to download the full source code of this [codeKata](https://github.com/inadram/CodeKata/tree/master/CheckOut) from my GitHub.