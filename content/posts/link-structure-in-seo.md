---
 url : "link-structure-in-seo"
 date : "2013-01-03"
 tags : 
  - "SEO" 
 title : "Link structure in SEO"
 description: "286818546204413953"
---

A website with a proper linking structure can help both Google and users to have better exploration experience and also help it to achieve a better visibility in search results ( Szymanski, Far, & Naumann, Sharing advice from our London site clinic, 2011).  
  

Google uses mature text-matching algorithms to return pages which are both relevant and important for each search query and links are one of the most important factors which can get pages “authority” and “importance”.  


In fact, Google consider a link between pages A to B as a vote from A to B where the importance of page A is carried to the page B as a link juice.  
  

Bear in mind that only natural links are useful and Google can distinguish the neutrality of the links by its algorithms. In other words Google consider the links which a website gains by its valuable content from other websites not the ones that it gets to fool a search engine (Google, Google-friendly sites, 2011) .  


Generally, links are categorised into three different types, “internal”, “outbound” and “inbound” links ( Ohye, Links information straight from the source, 2008).  


### Internal links  

Internal links are the ones that come from other pages of the same website (Google, Internal links, 2011). Most people will consider www.example.com and example.com as the same site while they are coming from a different subdomain, however, Google recently change its policy and treat the links that come from subdomains as internal links (Moskwa, Reorganizing internal vs. external backlinks, 2011).  

Linking related documents of a website internally is a great practice to help both users and Googlebots to explore all the content of a website, ( Szymanski, Far, & Naumann, Sharing advice from our London site clinic, 2011) (Ohye, Linking out: Often it’s just applying common sense, 2008) and also understand the importance of that page (Google, Internal links, 2011).  

### Outbound links  

Outbound links refer to the links which point to external websites. They allow us to explore the web while making it a collaborative and exciting place.  

Some webmasters use outbound links such as “Googlebomb” in order to make someone else’s website rank well for a certain query by linking to it excessively. For instance the official profile page of George Bush was ranked well for “miserable failure” term in Google (Mayer, 2005) while you could not find any of those words in that page at all (Beschloss & Sidey, 2009) .  

Google has recently minimising the impact of many Googlebombs but it shows how links can be powerful in Google’s algorithm (Moulton & Carattini, 2007).  

A group of webmasters are afraid of outbound links as they think that linking to external websites might cause their readers to leave their page or “leak out” their PageRank while Google use more mature algorithms in assigning PageRanks (Google, BlogHer 2007: Building your audience, 2007). Using the outbound links wisely can increase the credibility of a website as it shows that a website creator did research and shows that they have valuable knowledge in a particular subject. Relevant outbound links can help readers to get more in-depth information about similar subjects; also it can help a webmaster to make a relationship with other successful bloggers.  

In contrast, having hidden links or a large amount of unmonitored and promotional outbound links could confuse the visitors and reduce the credibility of the website (Ohye, Linking out: Often it’s just applying common sense, 2008) (Cutts, Hidden links, 2007).  

### Inbound link  

Links which come from external sites to a website are named as “inbound” links. Google considers merit-based inbound links which are assigning by websites editors voluntarily as a positive signal in its algorithm.  

In the same way Google analyses the content of the websites which are linked together by considering different metrics, such as “content’s relevancy” and “geographic location” of them to find out the quality of the link, and assign an appropriate PageRank to the target webpage according to the quality and importance of the link.  

In addition Google care about the position of the links in the website i.e. are they placed in the main page or footer? (GoogleWebmasterHelp, Are links in footers treated differently than paragraph links?, 2009) However, the orders of them in each section do not carry any extra points at all (GoogleWebmasterHelp, Is the order of links on a page important for SEO?, 2010).  

There are useful practices which can help webmasters to build natural links for their websites. Visitors tend to link a website if they find valuable content on it and webmasters try to attract their attention by uncovering new things such as, the latest news and insightful subjects or even use “controversial” strategy by starting arguments about people and things to get more links (GoogleWebmasterHelp, What are some effective techniques for building links?, 2010).  

Similarly participating positively in communities and blogs by offering knowledgeable reviews could help unknown webmasters to get more authority. Furthermore, registering the website in directories is another way to get inbound links (Szymanski, Quality links to your site, 2010) (Ohye, Good times with inbound links, 2008).  

On the other hand, buying or even using the links to pass PageRank is violating Google’s webmaster guideline (Google, Paid links, 2011), as is exchanging links in excessive reciprocal manner (link to me and I will link back to you!) or link exchanging with random websites and “web spammers”. These add almost nothing to the performance of the website over time and it can also have a negative effect on the ranking of the website in search result (Szymanski, Quality links to your site, 2010) (Google, Link schemes, 2011).  

Google suggest webmasters to use `“rel=nofollow”` attribute when they are have to link to spammy websites. This attribute won’t pass their reputation to that page and as a result Google won’t penalise them as a “nofollow” link is worth nothing for Google (GoogleWebmasterHelp, Are nofollow links irrelevant?, 2010).  

In other words, this attribute tells Googlebot to not follow that particular link and also to not pass any link juice to it, even if they come from well-established website e.g. Wikipedia.com (GoogleWebmasterHelp, Two questions about nofollow, 2009) (Google, Search Engine Optimization starter guide, 2010).  

The only websites that are exempt from the nofollow policy are well-known social networks such as twitter and facebook. Google still uses these social network sites as a signal in its algorithm (GoogleWebmasterHelp, Does Google use data from social sites in ranking?, 2010).  

Although there is no penalisation for the people who add a nofollow attribute to all of their links (GoogleWebmasterHelp, Will Google penalize sites which only link using the nofollow attribute?, 2011), Google suggest webmasters do not use it for their internal pages (GoogleWebmasterHelp, Should internal links use rel=”nofollow”?, 2011) as it can reduce their page ranks (Cutts, PageRank sculpting, 2009).  


### References

- *Szymanski, K., Far, P., & Naumann, S. (2011). Sharing advice from our London site clinic. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2011/04/sharing-advice-from-our-london-site.html)*
- *Cawrey, D. (2010, October 27). Why Isn’t My Site Moving Up in Google Rankings. Retrieved January 14, 2012, from tek3d: [[link]](http://tek3d.org/why-website-not-improve-google-ranking)*
- *Google. (2011). Google-friendly sites. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=40349)*
- *Ohye, M. (2008). Importance of link architecture. Retrieved January 7, 2012, from google webmaster central: [[link]](http://googlewebmastercentral.blogspot.com/2008/10/importance-of-link-architecture.html)*
- *Google. (2011). Internal links. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=138752)*
- *Moskwa, S. (2011). Reorganizing internal vs. external backlinks. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2011/08/reorganizing-internal-vs-external.html)*
- *Szymanski, K. (2010). Quality links to your site. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/06/quality-links-to-your-site.html)*
- *Mayer, M. (2005). Googlebombing ‘failure’. Retrieved January 7, 2012, from google blog: [[link]](http://googleblog.blogspot.com/2005/09/googlebombing-failure.html)*
- *Beschloss, M., & Sidey, H. (2009). George W. Bush. Retrieved January 7, 2012, from white house: [[link]](http://www.whitehouse.gov/about/presidents/georgewbush)*
- *Moulton, R., & Carattini, K. (2007). A quick word about Googlebombs. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2007/01/quick-word-about-googlebombs.html)*
- *Google. (2007). BlogHer 2007: Building your audience. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2007/03/blogher-2007-building-your-audience.html)*
- *Ohye, M. (2008). Linking out: Often it’s just applying common sense. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2008/10/linking-out-often-its-just-applying.html)*
- *Cutts, M. (2007). Hidden links. Retrieved January 7, 2012, from mattcutts: [[link]](http://www.mattcutts.com/blog/hidden-links/)*
- *GoogleWebmasterHelp. (2009). Are links in footers treated differently than paragraph links? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=D0fgh5RIHdE)*
- *GoogleWebmasterHelp. (2010). Is the order of links on a page important for SEO? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=A6uRKvnrsnE)*
- *Szymanski, K. (2010). Quality links to your site. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/06/quality-links-to-your-site.html)*
- *GoogleWebmasterHelp. (2010). What are some effective techniques for building links? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=MkLFlaWxgJA)*
- *Ohye, M. (2008). Good times with inbound links. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2008/10/good-times-with-inbound-links.html)*
- *Google. (2011). Paid links. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=66736)*
- *Szymanski, K. (2010). Quality links to your site. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/06/quality-links-to-your-site.html)*
- *Google. (2011). Link schemes. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=66356)*
- *GoogleWebmasterHelp. (2010). Are nofollow links irrelevant? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=g37bwBlifnk)*
- *GoogleWebmasterHelp. (2009). Two questions about nofollow. Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=x4UJS-LFRTU)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *GoogleWebmasterHelp. (2010). Does Google use data from social sites in ranking? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=ofhwPC-5Ub4)*
- *GoogleWebmasterHelp. (2011). Will Google penalize sites which only link using the nofollow attribute? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=mg1A5wF3Ac4)*
- *GoogleWebmasterHelp. (2011). Should internal links use rel=”nofollow”? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=bVOOB_Q0MZY)*
- *Cutts, M. (2009). PageRank sculpting. Retrieved January 7, 2012, from mattcutts: [[link]](http://www.mattcutts.com/blog/pagerank-sculpting/)*