---
 url : "csharp-example-adapter-pattern"
 date : "2013-01-28"
 tags : 
  - "Programming"
  - "Design pattern"
 title : "CSharp example of Adapter design pattern"
 description: "295701613157425152"
---

### What is it?
It adapts your client code interface with a library interface. Sometimes we call adapters as wrappers. This design pattern can help us to achieve open/close principal in SOLID design.

### Example

#### Class Library
In the code below, we have the class library that get the machine detail and then print it in a special format.

```
public class Machine : IMachine {
	private readonly IPrinter _printer;
	public string Output;
	public Machine(IPrinter printer) {
		_printer = printer;
	}
	public void PrintMachineName(IMachineDetail machineDetail) {
		Output = _printer.GenerateMchineNameFormat(machineDetail);
		Console.WriteLine(Output);
	}
}
``` 
   
#### Client code
On the other hand, our client likes to get the list of machine names in below format.

```
class Fax {
	public string GetFaxName(List<MachineDetail> details) {
		return details.ToString();
	}
}
```

#### Adapter  
    
As you can see, client class cannot use our library to print the machine name, because they have different input and output.

In order to adapt our class library to the client, we can write a third party class to use our library functionality and pass it to the client in appropriate format.

1. Create an interface  

First we need to create an interface for our adapter class and pass it into our client code.

```
public interface IMachineAdapter {
	string GetFaxName(List<MachineDetail> details);
}
 
class MachineAdapter : IMachineAdapter {
	public string GetFaxName(List<MachineDetail> details) {
		return null;
	}
}
``` 

2. Inject into the client class  

Then we need to pass the interface as a dependency to our client constructor and ask our desired method to call the new method.

```
class Fax {
private readonly IMachineAdapter _machineAdapter;
	public Fax(IMachineAdapter machineAdapter) {
		_machineAdapter = machineAdapter;
	}

	public string GetFaxName(List<MachineDetail> details) {
		return _machineAdapter.GetFaxName(details);
	}
}
```
    
3. Implement adapter class  

In this step, we are going to make adapter that accept our client input and use the class library functionality, then generate the result that can be used in the client side.

```
class MachineAdapter : IMachineAdapter {
Machine _machine;
public string GetFaxName(List<MachineDetail> details) {
string allFaxMachineNameInFormat=string.Empty;
	foreach (var machineDetail in details) {
		_machine = new Machine(new Printer());
		_machine.PrintMachineName(machineDetail);
		allFaxMachineNameInFormat += _machine.Output;
	}
	return allFaxMachineNameInFormat;
	}
}
```

It will now iterate through all the list elements and return the machine names in special string format.

### Benefits

It can help us to make a module open to extension but close to the modification (Open/close principal)
The adapter class will wrap our needed functionality into an interface. In other words, using the wrapper class rather than just implement it in the method will help us to increase code re-usability and decrease duplication.
It gives us opportunity to write different wrapper classes in different situation and change the code expected behavior according to our needs.

### Download
Feel free to download this Csharp adapter pattern example from my [GitHub](https://github.com/inadram/Design-Pattern/tree/master/AdapterPattern)