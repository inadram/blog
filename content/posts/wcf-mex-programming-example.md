---
 url : "wcf-mex-programming-example"
 date : "2013-01-21"
 tags : 
  - "Programming"
  - "WCF"
 title : "WCF Mex programming example"
 description: "294010381716840448"
---

It is possible to walk to the server and get the contract endpoints of it at run time. It will help us to have more robust classes which let client dynamically choose the desire endpoints.

In this case firstly you should add the MEX endpoint to your server side application.

```
<endpoint address="http://localhost:8080/ServiceMachine/mex"
	binding="mexHttpBinding" bindingConfiguration="" name="mex"
	contract="IMetadataExchange" />
``` 

The only difference is this mex endpoint use IMetadataExchange contract .

Then we can import this mex address to our client application

By the use of MetadataResolver class your can discover that mex endpoint and retrieve all the service endpoint and use your desire one like below

```
ServiceEndpointCollection serviceEndpointCollection = 
	MetadataResolver.Resolve(typeof(IMachine),
 new EndpointAddress("http://localhost:8080/ServiceMachine/mex"));
	foreach (var endpoint in serviceEndpointCollection) {
		var channel = new MachineClient(endpoint.Binding, endpoint.Address);
		string machineName = channel.GetMachineName(new MachineDTO { MachineID = "1", MachineName = "test" });
		Console.WriteLine(machineName);
		Console.WriteLine(endpoint.Binding);
	}
```

### Download
Feel free to download the source code of [WCF Mex programming from my GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF/WCFService/ClientMax).