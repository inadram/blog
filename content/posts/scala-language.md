---
 url : "scala-language"
 date : "2014-10-10"
 tags : 
  - "Programming"
  - "scala"
 title : "Scala Language by example"
---
### What is Scala?
it is a language that  is invented by Martin Odersky  and runs on the JVM (Java Virtual Machine) and helps you to write  beautiful and powerful code in type-safe way.

### Why Scala?
It has Flexible syntax with Unified type system (no primitive). 
It has Type inference which automatically assign right value type to an expression.  
It has extended OO support by adding trait and case classes to it.  
it has functional programming feature such as pattern matching, higher order functions, lambdas, lazy evaluation and tail recursion.
#### Install Scala
You can install Scala with homebrew

```
brew install scala
```

### SBT
sbt is a simple build tool that can build the project and manage the dependencies. It can do continuos compilation, testing and deployment. For instance it can watch a folder and when it detect a change automatically build the project.

It can be install with homebrew
```
brew install sbt
```
### IDE
you can use Intelji and eclips to run and debug scala.
in Intelij you need to install Scala plugin and also set the Scala and JVM path to it

```
//run "which scala" to get the path
/usr/local/Cellar/scala/2.[version]/libexec
```