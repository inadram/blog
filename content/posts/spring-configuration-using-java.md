---
 url : "spring-configuration-using-java"
 date : "2014-05-16"
 tags : 
  - "Programming"
  - "Java"
  - "Spring"
 title : "Spring configuration using Java"
 description: "467228230609408001"
---

### What is it ?
It is easier for developers to bind java beans with pure java and avoiding from using xml config files.

From Spring 3.0 onward most features of Spring can be configure just with Java.

### How to use it ?
It is looking for the files that have @configuration annotation rather than xml config files.
`@Bean` will be used for method level annotation.
Spring does not care for the class or method names and they can be anything.

#### Configuration file  

> configuration file

```
@Bean(name="customerService")
public CustomerService getCustomerService(){
    return new CustomerServiceImplementation();
}

@Bean(name="customerRepository")
public CustomerRepository getCustomerRepository(){
    return  new hibernateRepositoryImplementation();
}
```

By using java configuration file the mystery of injection goes away.
It let the compiler know what type of concrete class that it should use when it want to make instance of each class from the interface.

#### Inject the dependencies
we need to inject the customer repository into customer service class to make it easy for writing tests.

> customer service

```java 
public class CustomerServiceImplementation implements CustomerService {
 
    private CustomerRepository customerRepository = new hibernateRepositoryImplementation();
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
}
```

We can use simple setter injection to pass the dependency into it

> pass dependency into customer service

```java
public class CustomerServiceImplementation implements CustomerService {
 
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
 
    private CustomerRepository customerRepository ;
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
}
```

#### Update config file
Now we need to update the configuration file and let it know that it have to pass this dependency while creating the customer service class.

> update config file

```
@Bean(name="customerService")
public CustomerService getCustomerService(){
        CustomerServiceImplementation customerServiceImplementation= new CustomerServiceImplementation();
        customerServiceImplementation.setCustomerRepository(getCustomerRepository());
 
        return customerServiceImplementation;
}
```
### Calling it
We need to pass the configuration class name into `AnnotationConfigApplicationContext` constructor.

```
ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
```

Similar to XML approache we need to get the beans and calling our method

> get beans

```
CustomerService service = appContext.getBean("customerService", CustomerService.class);
System.out.println(service.findAll().get(0).getFirstName());
```
        
customerService is the name of our annotation that we had used in our config file.

### Download
Feel free to download the full source code of this example from my [github](https://github.com/inadram/Code-Sample/tree/master/Java/Spring/spring-java-example).
