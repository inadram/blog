---
 url : "soap-vs-rest-in-wcf"
 date : "2013-01-23"
 tags : 
  - "Programming"
  - "WCF"
 title : "SOAP VS REST in WCF"
 description: "294059149426307073"
---

### What is SOAP?
It’s a model to define high level applications that support various protocols such as “HTTP”,”TCP” and “MSMQ” with only POST method.

#### Benefits :
It has high interoperability and useful for enterprise system as we have more control over the system .  

High security maneuver by using WS-Security models.  

There are lots of tools and support to generate the client side code .

### What is REST?
REST refers to Representational State Transfer is an architecture that our current web is basically working.REST uses HTTP methods such as POST and GET. It can transfer data such as HTML, JSON , XML , etc.

#### Benefits:
It is easy to extend the project as it has high scalability potential.  

It can interact with all the application that support HTTP and XML .  

It won’t cause any unsafe side-effect in retrieving data as it uses “GET” which is a standard web protocol.

### REST vs. SOAP
You always interact with your resources through “HTTP” methods.

```
Machine{}
```

For instance to get a Machine you Use “GET” method , to submit a Machine use “POST” , to update a Machine use “PUT” protocols and use “Delete” operation to remove it.

SOAP emphasizes on the actions and verbs.
```
GetMachineName()

SubmitMachine()
```

If you try to invoke these messages through “HTTP” it always uses “POST” method.

### Summary

REST is good for the project that HTTPS can support the security of the system and we are not in complex enterprise projects that need to deal with some COM+ apps.

