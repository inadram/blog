---
 url : "comparing-search-engine"
 date : "2012-12-07"
 tags : 
  - "SEO" 
 title : "Comparing major search engines usage"
 description: "277859381293498368"
---

There are numerous search engines in the internet but only some of them were successful in attracting users. According to the comScore statistic Google and Yahoo stand in the first position by 65.3% and 15.5% respectively and Microsoft is in third place by about 14.7% in 2011 (Reston, 2011).

|Entity|Percentage in USA 2011|
|------|----------------------|
|Google Sites|65.3%|
|Yahoo! Sites|15.5%|
|Microsoft Sites|14.7%|
|Ask Network|3.0%|
|Aol, Inc.|1.5%|
|Total|100%|
> Table 1 Search engines popularity in USA 2011</td></tr>


This segment of the study endeavors to shed light on the related factors that we might consider regarding the most important search engine i.e. Google.

There are over two hundred signals which Google cares about them in ranking websites while scoring their respective search result (Falls, Goradia, & Perez, 2010) but there are only limited numbers of factors which are officially cited by Google as a guideline for webmasters. Although these tips won’t bring a website to first rank automatically, it helps Google to “crawl”, “understand” and “index” the website easier.

### References
- *Reston, V. (2011). comScore Releases September 2011 U.S. Search Engine Rankings. Retrieved January 7, 2012, from ComScore: [[1]](http://www.comscore.com/Press_Events/Press_Releases/2011/10/comScore_Releases_September_2011_U.S._Search_Engine_Rankings)*
- *Falls, B., Goradia, A., & Perez, C. (2010). Google’s SEO Report Card. Retrieved January 7, 2012, from Google Webmaster Central: [[2]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/google-seo-report-card.pdf)*