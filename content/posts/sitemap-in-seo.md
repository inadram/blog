---
 url : "sitemap-in-seo"
 date : "2012-12-19"
 tags : 
  - "SEO"
 title : "Sitemap in SEO"
 description: "282103642033639424"
---

Google in 2005 undertook an experiment i.e. “Sitemaps (Sitemaps, 2008) to make the web a better place for users and webmasters as it can help Google to improve the freshness and coverage of its index (Shivakumar, 2005).

Google (Google, About Sitemaps, 2011) suggested to the webmasters to use sitemaps if their websites has

- Dynamic content
- Few links to it
- A large archive of content but are not links together properly
- Have pages that are not easily get crawled by Googlebot.  

There are two types of sitemaps, one is a “HTML sitemap” (lower-case) which simply displays the structure of the site to the visitors in hierarchical ways and helps them to find the pages easier and the other one is a “XML Sitemap” (upper-case) which helps Google to discover the pages of a website quicker (Google, Search Engine Optimization starter guide, 2010) .

|{{< figure src="/media/23.jpg" >}}|
|---|
|Figure 14: HTML and XML sitemap (Google, 2010)|

Webmasters can create XML files which has list of the URLs of their website pages that they want to be crawled by Google with optional hints such as their important pages and their change frequency (Sullivan, 2005) (GoogleWebmasterHelp, Will I be penalized if my URLs all have the same priority?, 2009) .  

According to the latest version of sitemaps.org, a website could have up to 1,000 sitemaps that each one can contain up to fifty thousand URLs. Although Google does not grantee to index all of them, at least it helps Google to understand the content of the website and increase its visibility in search engines (sitemaps, 2008) (Moskwa & Foucher, Sitemaps FAQs, 2008).
Nowadays, Google support various types of the sitemaps such as XML, RSS 2.0, Atom 1.0, Text files, Media-RSS feeds to support its “video search”, “code search”, “mobile search”, “geo-data” and “news search” (Mueller, Sitemap Submission Made Simple, 2008) (Simon, 2010).  

Although sitemaps could not have an effect on the ranking of a website directly, they are great resources for search engines as they can highlight the important content for crawlers to discover them quickly (Evlogimenos, 2010), as well as HTML sitemap could help websites to redistribute their page rank between their pages, especially in web 2.0 industries (GoogleWebmasterHelp, Is it still important to offer a site map to users?, 2010).
Although HTML sitemaps are better than XML ones, webmaster should organise them properly and not allow them become outdated sitemap with broken links (GoogleWebmasterHelp, Which is better: an HTML site map or XML Sitemap?, 2009) (Google, Search Engine Optimization starter guide, 2010).

### References

- *Sitemaps. (2008). What are Sitemaps? Retrieved January 7, 2012, from Sitemaps: [[link]](http://www.sitemaps.org/)*
- *Shivakumar, S. (2005). Webmaster-friendly. Retrieved January 7, 2012, from google blog: [[link]](http://googleblog.blogspot.com/2005/06/webmaster-friendly.html)*
- *Google. (2011). About Sitemaps. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=156184)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Sullivan, D. (2005). New “Google Sitemaps” Web Page Feed Program. Retrieved January 7, 2012, from search engine watch: [[link]](http://searchenginewatch.com/article/2061916/New-Google-Sitemaps-Web-Page-Feed-Program)*
- *GoogleWebmasterHelp. (2009). Will I be penalized if my URLs all have the same priority? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=JfOk0gjNE5Y)*
- *Moskwa, S., & Foucher, T. (2008). Sitemaps FAQs. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2008/01/sitemaps-faqs.html)*
- *Mueller, J. (2008). Sitemap Submission Made Simple. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2008/12/sitemap-submission-made-simple.html)*
- *Simon, J. (2010). Sitemaps: One file, many content types. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/06/sitemaps-one-file-many-content-types.html)*
- *Evlogimenos, A. (2010). Adding Images to your Sitemaps. Retrieved January 7, 2012, from google webmaste rcentral blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/04/adding-images-to-your-sitemaps.html)*
- *GoogleWebmasterHelp. (2010). Is it still important to offer a site map to users? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=t5LIlkhxl2s)*
- *GoogleWebmasterHelp. (2009). Which is better: an HTML site map or XML Sitemap? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=hi5DGOu1uA0)*