---
 url : "add-bootstrap-to-mvc"
 date : "2012-10-28"
 tags : 
  - "front end" 
  - "Programming" 
 title : "How to add Bootstrap to MVC project"
 description: "262659426421469186"
---
### what is Bootstrap ?

it is a front-end framework which helps web developer to use semi ready css and javascript functionality .

### How to use Bootstrap in MVC project ?

There are two possible solution in doing it .

#### first and easier one is :

1.download [MVC 4 TwitterBootsrap](http://visualstudiogallery.msdn.microsoft.com/268d0b05-6ba5-4793-9a10-7d9d2a478881) patch and install it via your Nuget manager which exist in Visual studio.

2.adding the Bootstrap Layout to the project and update your `_ViewStart` file to use it .

Thats it ! but if you need to have more control over your project you can use the second solution.

#### The second solution is :

1.Tick your desire functionality and download the package from Bootstrap’s GitHub page.

2.Add them to your MVC project by keeping the folder path and file names.

> if you want to scatter the files or change the name , make sure you are updating the img file path which exist in the css files.

3.Make sure that you are using the latest `Jquery` library by updating your Nuget package.

4.open the BundleConfig.cs file which exist in `App_start` folder and try to the files by below syntax.

```
bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/css/bootstrap.css"));
bundles.Add(new ScriptBundle("~/Content/js").Include("~/Content/js/bootstrap.js", "~/Content/js/jquery.js"));
```
> my files are exist in “Content” folder.

5.open your `_layout.cshtml` file and add the files by below syntax

```
@Styles.Render("~/Content/css")
@Scripts.Render("~/Content/js")
```
### is there any other cool thing like bootstrap on internet ?!

1.Yes, and I highly recommend [FontAwesome](http://fortawesome.github.com/Font-Awesome/) which you can add it and use it beside bootstrap.

2.you can easily customise your css with [stylebootstrap.info](http://stylebootstrap.info/) and [Bootstrap Button maker](http://charliepark.org/bootstrap_buttons/) without having any CSS knowledge .

3.Or even download some ready css from [bootswatch.com](http://bootswatch.com/)

How my website will be look like ?

I just design [RetweetLine.com](http://retweetline.com/) with fontAwesome and bootstrap and some customisation