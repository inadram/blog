---
 url : "value-type-reference-type-csharp-example"
 date : "2013-02-22"
 tags : 
  - "Programming"
  - "C#"
  - "example"
  - "type"
 title : "Value Type Vs Reference Type in cSharp example"
 description: "304979861661949954"
---

### What is Reference type ?
When we have reference type  the value inside the object is pointing to the address in the memory. Multiple variable can point to the same address in the memory . Also it is possible to change the memory of the address that variable pointing to it in its lifetime.

Object, Classes and some variable types such as String are reference type.

#### Example
If you have someDto classes

> some dto

```
public class someDto {
	public int Number;      
	public string PropertyString { get; set; }
}
```    
And initialising it in a separate method

> initialise DTO

```
private void InitialiseDto() {
	myNumber = 10;
	myPropertystring = "Test property string";
	myDto = new someDto(); 
	myDto.Number = myNumber;
	myDto.PropertyString = myPropertystring;
}
```

You can change the value of Dto by passing it to some ChangeValue method

> change value

```
public void ChangeValue(someDto dto) {
	dto.Number = dto.Number*2;
	dto.PropertyString = dto.PropertyString + dto.PropertyString;
}
```

> calling the method

```
InitialiseDto();
ChangeValue(myDto);
```
##### What will be happening ?
Both myDto and dto objects are pointing to the same reference address in the memory therefore if you change one of them it will be change the other one as well.

#### Immutable types
Some reference types such as string is immutable which means that you cannot change their value after initializing them.

##### Example
If you have some method to change the value of string

> change string value

```
public void ChangeValueOfString(string str) {
	str.Remove(0, 1);
}
```

And you are passing some String object to it , it won’t change the value of the string .

> passing some string

```
char[] strAlphabet={'A','B','C'};
string str = new string(strAlphabet);
string strpassing = str;
var cSharpTypes = new cSharpTypes();
cSharpTypes.ChangeValueOfString(strpassing);
```            
in these cases we can use similar mutable reference types such as StringBuilder class.

### What is Value type?
They hold a variable and do not have a pointer to the heap. They are lightweight and immutable . variables such as ‘int’ , ‘double’, ‘decimal’ and also ‘struct’ are value type.

#### Example
If you have some struct like below

> struct

```
public struct someStruct {
	public int Number; 
	public string StringValue;     
}
```    
And try to change its value by initializing it .

> initialize struct

```
myNumber = 10;       
myStringValue = "Test String";          
someStruct = new someStruct(); 
someStruct.Number = myNumber;
someStruct.StringValue = myStringValue;
```            
And passing it ChangeValue method

> Change value

```
public void ChangeValue(someStruct _struct) {
	_struct.Number = _struct.Number * 2;
	_struct.StringValue = _struct.StringValue + _struct.StringValue;
}
```
        
We will see that the value won’t be changed , as the object are value types and they are not aware of changes that happen to each other.

#### Ref  type

If you wish that your value type have reference type attribute you can pass them with ‘Ref’ keyword. It will pass a reference of our value type to the method parameter .

##### Example

> ref type method

```
public void ChangeValue(ref someStruct _struct) {
            _struct.Number = _struct.Number * 2;         
            _struct.StringValue = _struct.StringValue + _struct.StringValue;
}
```        
To using `‘Ref’` keyword we have to initialize the passing type before calling the method.

> initialise structure

```
myNumber = 10;
myStringValue = "Test String";
myPropertystring = "Test property string";
someStruct = new someStruct(); 
someStruct.Number = myNumber;
someStruct.StringValue = myStringValue;
```
> change value

```
 ChangeValue(ref someStruct);
``` 
#### Out Type
`‘out’` type is similar to `‘Ref’` in making reference of value type . We have not had to initiate our value type before calling the method but we have to initialize it inside the method .

Therefore it is useful when we want to ask a method to make value type objects and return it to us.

##### Example
we should prefix our input parameter with out keyword.

> out type method

```
public void ChangeValueOut(out someStruct _struct) {
            _struct.Number = 0;   
            _struct.StringValue = string.Empty;
}
```
        
and passing our value type object with out prefix as well.

> calling out type method

```
ChangeValueOut(out someStruct);
```

### Download
Feel free to download the full source code of this example from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/C%23/cSharpTypes).

