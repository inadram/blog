---
 url : "getting-free-retweet"
 date : "2012-12-06"
 tags : 
  - "products"
  - "RetweetLine" 
 title : "Beta version of getting free retweet is now live on RetweetLine.com"
 description: "276739275171717121"
---
[Retweetline.com](http://web.archive.org/web/20150918220526/http://retweetline.com/) is now available to serve [free retweets](http://web.archive.org/web/20150918220526/http://retweetline.com/) to its user. The service is 100% free for now .

### How to SignUp?
You can sign up with your twitter account . it is one click signup and we get all require information from twitter . we do not ask your twitter password and all your sensitive data will be remain at your twitter account .

### How to use ?
after you login we fetch your latest public tweets and store them in our database . these statuses will be shown to other users.  we fetch latest 5 statuses and exclude the status that you have retweeted . therefore you have more chance to get twitter credit and become a twitter recognize user .

You can also ,sync your latest tweets with our database by clicking on sync now button.

{{< figure src="/media/retweetline.jpg">}}

### Gaining points
when you sign up , we give you 10 points as gift . Also you can gaining points by retweeting other statuses through. We order the statuses according to their points. in other words statuses with higher point are coming on top . Also you can ignore status which will be hide them.  

### Assigning Points
By default you give 2 points to the users who are retweeting your status. but if you wish more users see your status you can increase your given point up to 10.  

This is beta version only ! so we are happy to get your feedback so it can help us to drive it to your expectation. 