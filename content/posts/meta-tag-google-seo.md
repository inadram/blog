---
 url : "meta-tag-google-seo"
 date : "2012-12-09"
 tags : 
  - "SEO" 
 title : "What Google says about Meta tags in SEO"
 description: "278457559696945152"
---
Google cares about some of Meta tags such as “description”, “robots” and “Google-site-verification” and ignores the rest of them (Google, Meta tags, 2011).
Google claims that although some of its products such as “Google Search Appliance” use Meta keywords tag in matching the queries, it has not taken into account this tag in its main web search ranking (Cutts, Google does not use the keywords meta tag in web ranking, 2009).
At the same point, Google clearly ignores the meta description tag as a signal in its ranking algorithm (Falls, Goradia, & Perez, 2010). However, Google suggests to webmasters to write unique and accurate summary of the page’s content, or at least for their important pages in their meta description, to increase their click to rate percentage from search result page (Google, Changing a site title and description, 2011) (Google, Search Engine Optimization starter guide, 2010).

Webmasters can also add to their webpages other useful information such as their events dates (Google, Events, 2012), people (Google, People, 2011), reviews (Google, Reviews, 2012) recipes (Google, Recipes, 2012) and also their products details (Google, Products, 2012). Google sometimes uses these descriptions in the snippet of search result which can help both users and search engines to understand what the page is mainly about and also direct users to good results faster, by decreasing the click-and-backtrack behavior which usually frustrating users (Google, Changing a site title and description, 2011).
Webmasters can benefit from their human-readable descriptions in improving the quantity and quality of their traffic, as the quality of snippet has a direct impact on a website being clicked (Krishnan, 2007).
On the other hand, users avoid clicking on the website which has empty description as they think that the page is spam, is down or even it is not the official page which they are looking for (Falls, Goradia, & Perez, 2010).

### Reference
- *Falls, B., Goradia, A., & Perez, C. (2010). Google’s SEO Report Card. Retrieved January 7, 2012, from Google Webmaster Central: [[1]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/google-seo-report-card.pdf)*
- *Google. (2011). Meta tags. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[2]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=79812)*
- *Cutts, M. (2009). Google does not use the keywords meta tag in web ranking. Retrieved January 7, 2012, from Google webmaster central blog: [[3]](http://googlewebmastercentral.blogspot.com/2009/09/google-does-not-use-keywords-meta-tag.html)*
- *Google. (2011). Changing a site title and description. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[4]](http://www.google.com/support/webmasters/bin/answer.py?answer=35624)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[5]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Google. (2012). Events. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[6]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=164506)*
- *Google. (2011). People. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[7]](http://www.google.com/support/webmasters/bin/answer.py?answer=146646)*
- *Google. (2012). Reviews. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[8]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=146645)*
- *Google. (2012). Recipes. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[9]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=173379)*
- *Google. (2012). Products. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[10]](http://www.google.com/support/webmasters/bin/answer.py?answer=146750)*
- *Krishnan, R. (2007). Improve snippets with a meta description makeover. Retrieved January 7, 2012, from google webmaster central blog: [[11]](http://googlewebmastercentral.blogspot.com/2007/09/improve-snippets-with-meta-description.html)*
