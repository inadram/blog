---
 url : "wcf-restful-example"
 date : "2013-01-25"
 tags : 
  - "Programming"
  - "WCF"
 title : "WCF RESTFul service example"
 description: "294796207056887808"
---

Rest stand for `REpresentational State Transfer` that builds on HTTP methods on different data format.

Visual Studio config our binding according to `SOAP` architecture . We have to configure to allow WCF work with `RESTful` design.

### Server side
In this example we have three main methods, i.e. AddMachine to submit a machine , `GetMachineNameById` to get the machine name with GET method and `DeletemachineById` to delete a machine.
```
[ServiceContract]
public interface IMachine {
	[OperationContract]
	[WebInvoke(Method = "POST", UriTemplate = "machine")]
	string AddMachine(MachineDetail machineDetail);

	[WebGet(UriTemplate = "machine/{machineId}")]
	[OperationContract]
	string GetMachineNameById(string machineId);

	[WebGet(UriTemplate = "machines")]
	[OperationContract]
	string GetMachineNames();

	[WebInvoke(UriTemplate = "machine/{machineId}",Method = "DELETE")]
	[OperationContract]
	string DeleteMachineById(string machineId);
}
``` 

As we want they will be accessible in client side we have to add `[ OperationContract ]` to them.

For GET type methods we use `WebGet` and with `UriTemplate` we will match the its value with input variable of the method.

For `POST` and `DELETE` methods we use WebInvoke attribute and same scenario for `UriTemplate`.

```
[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
public class Machine : IMachine {
List<MachineDetail> MachineDetails=new List<MachineDetail>();
private int machineId = 0;
	public string AddMachine(MachineDetail machineDetail) {
		machineDetail.ID = (++machineId).ToString();
		MachineDetails.Add(machineDetail);
		return (string.Format("add successfully {0}",machineDetail.Name));
	}

	public string GetMachineNameById(string machineId) {
		MachineDetail machineDetail = MachineDetails.Find(x => x.ID == machineId);
		return machineDetail.Name;
	}
 
	public string GetMachineNames() {
		string machine=string.Empty;
		foreach (var machineDetail in MachineDetails) {
		string name = machineDetail.Name;
		string id = machineDetail.ID;
	 machine+=
		string.Format("ID {0} and name is {1} <br/>",id, name);
	}
	return machine;
}

public string DeleteMachineById(string machineId) {
MachineDetails.RemoveAll(x => x.ID == machineId);
return string.Format("machine with Id {0} has successfully deleted",machineId );
}
}
```   

In the implementation section we can add single `InstanceContextMode` behaviour to the class so it will only create one instance of the class and we can add and fetch from the list without making new instance of it .

### Host
There are two ways also to set up host.

### App.Config file
Add `endpointBehavior` to use `webHttp`

```
<endpointBehaviors>
	<behavior name="web">
		<webHttp/>
	</behavior>
</endpointBehaviors>
```
     
Update the endpoint to use `webHttpBinding` and `web` behavior like below

```
<endpoint address="" binding="webHttpBinding" contract="RESTWcf.IMachine" behaviorConfiguration="web"></endpoint>
``` 

`WebServiceHostClass` make it really simple as do not have to define the bindings and meta behaviors in the config file to run the server. We pass into it the type of our server class with the base url

```
var webServiceHost=new WebServiceHost(typeof(Machine),new Uri("http://localhost:8080/RESTWcf"));
webServiceHost.Open();
Console.ReadLine();
webServiceHost.Close();
```
 

### Testing it
To testing it we can use `httputil.js` that I have put in the solution to submit a pocket to the server and delete it. 
Also it is possible to use web browser to fetch the data from the endpoint.

#### Submit and Delete data
We can use WScript.exe that exist in `C:\Windows\System32` to submit javascript `POST`.

We can create simple xml file and name it `Machine.xml`

```
<MachineDetail xmlns="http://inadram.com/machine">
	<Name>test machine</Name>
</MachineDetail>
```

And try to submit it with below the command line

```
wscript.exe httputil.js POST http://localhost:8080/RESTWcf/machine  Machine.xml
```

And also delete by invoking a delete method on it with desired ID at the end of URL

```
wscript.exe httputil.js Delete http://localhost:8080/RESTWcf/machine/1   Machine.xml
```
#### GET data
You can get data by browsing to the base url which in this case you can retrieve all submitted data from

```
http://localhost:8080/RESTWcf/machines
```

Or if you are interested in one of them pass its id at the end of the URL like below

```
http://localhost:8080/RESTWcf/machines/1
```

### Download
Feel free to download the source code of this example from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF/RESTWcf).
