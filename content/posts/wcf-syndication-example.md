---
 url : "wcf-syndication-example"
 date : "2013-01-28"
 tags : 
  - "Programming"
  - "WCF"
 title : "WCF syndication example"
 description: "295949525850394624"
---

### What is it ?
You can ask your WCF service to represent data in ATOM or RSS format .

### Example
#### Defines feed contract
Firstly we need to define syndication contract .

ServiceKnownType: indicate that what is the return type of our desired feed will be look like . In this example we expect to get ATOM or RSS.

OperationContract: to make it accessible to the world ! We have to add this attribute to the method

WebGet: the method use HTTP/GET type and return SyndicationFeedFormatter.

> contractC#

```
[ServiceKnownType(typeof(Atom10FeedFormatter))]
[ServiceKnownType(typeof(Rss20FeedFormatter))]
[OperationContract]
[WebGet(UriTemplate = "machine/feed/{format}")]
SyndicationFeedFormatter GetFeed(string format);
```    
        
#### Implement it
Then we need to implement this method to return appropriate values.

We use SyndicationFeed and set up “Title” and “Content” values by using TextSyndicationContent.

Then we are going to add some item to our feed. In this case, we use LINQ and loop through MachineDetails List to set machine Id and name.

> implementationC#

```
SyndicationFeedFormatter syndicationFeedFormatter;
var syndicationFeed=new SyndicationFeed {
	Title = new TextSyndicationContent("Machine feed title"),
	Description = new TextSyndicationContent("Machin feed description"),
	Items = from machine in MachineDetails
	select new SyndicationItem() {
		Title = new TextSyndicationContent(machine.Id),
		Content = new TextSyndicationContent(machine.name)
	}
};
```
                                    
#### Return feed
After making the feed value, we should decide on the feed format as well. In this case we can return ATOM or RSS according to our client needs.

> feed typeC#

```
if(format.Equals("Atom")) {
	syndicationFeedFormatter = new Atom10FeedFormatter(syndicationFeed);
 } else {
  syndicationFeedFormatter = new Rss20FeedFormatter(syndicationFeed);
 }
``` 
           
#### Testing it.
Simply we can invoke it by running through the URL that we are hosting the feed i.e. “http://localhost:8080/wcf/machine/feed/Atom”.

### Download
Feel free to download full source code of this WCF syndication feed example from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF/WCFSyndication).