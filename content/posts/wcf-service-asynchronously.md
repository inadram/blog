---
 url : "wcf-service-asynchronously"
 date : "2013-01-21"
 tags : 
  - "Programming"
  - "WCF"
 title : "Call WCF service methods asynchronously example"
 description: "293649996866260992"
---
It is possible to call wcf service methods asynchronously so it can improve the performance .

### How to do it ?
Go to the service configuration that you have already imported and then enable the asynchronous option.

- Service reference
- configure service reference
- allow generation of asynchronous operations

Then it will be regenerate your client side methods and you will see new Async type method.

Async type method do not have return type and basically you trigger a method when their job is completed

```
var channel = new MachineClient("BasicHttpBinding_IMachine");
channel.GetMachineNameCompleted+=ChannelOnGetMachineNameCompleted;
channel.GetMachineNameAsync(new MachineDTO {MachineID = "1",MachineName = "async machine"});
Console.WriteLine("some other operation is going on ...");
Console.ReadLine();
```            
```
private static void ChannelOnGetMachineNameCompleted(object sender, GetMachineNameCompletedEventArgs getMachineNameCompletedEventArgs) {
	string result = getMachineNameCompletedEventArgs.Result;
	Console.WriteLine(result);
}
```        

In this example you asked the service to trigger the `ChannelOnGetMachineNameCompleted` event handler to execute once the async method complete its job.

### Result

As you can see in the result the next operation is going on and do not wait for the async method to be completed.

### Download
Feel free to download the full source code of this asynchronous wcf service call from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF/WCFService/AsyncClient).
