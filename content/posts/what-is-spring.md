---
 url : "what-is-spring"
 date : "2014-03-30"
 tags : 
  - "Programming"
  - "Java"
  - "Spring"
 title : "What is spring?"
 description: "461644132687937537"
---

Spring is a framework to reduce Enterprise Java Development complexity.
it is Plain Old Java Object based which means that any code you written in spring can write without it as well !
It is a lightweight and unobtrusive framework in compare with older J2EE if you using it in a right way.
It support AOP framework (Aspect oriented programming)
### Structure
The architecture of spring is based on design patterns which can help us to use best practices without having too much in dept knowledge about them.

- Singleton
- Factory
- Abstract Factory
- Template Method
- Annotation based configuration
 
### History
on **2003** **Milestone** was released

on **2004** version **1.0** was released

on **2006** version **1.2.6**  was released.  it focus on productivity and SOLID code principals

on **2007** version **2.5** was released. it support annotation and xml development

on **2009** version **3.0** was released it support web services

on **2014** version **4.0.3** was released. It support web Socket ,java 8,  etc.

### How can it help me ?
- Increase testability
- Increase Maintainable
- Help us to improve scalability of application by decoupling layers and also adding catch layer etc
- Reduce complexity over code
- Put focus on business by making everything easier and developing code faster
- Remove configuration and lookup codes
- Code can focus on testing
- It help us to do annotation or XML based development
