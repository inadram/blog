---
 url : "search-engine-types"
 date : "2012-11-29"
 tags : 
  - "SEO" 
 title : "What are the Search engine Types"
 description: "274194393228005377"
---
### What are the Search engine Types
Nowadays, search engine go beyond retrieving and showing information, and they could even have an effect on various social and financial dynamics such as politic and the economic, by showing bias result to the end user. Google bombing is a good example of political use of search engines (Zeller, 2006).

Generally there are four types of search engines namely, crawl based, human-powered directories, semantic search and hybrid search engines.

#### Crawl based
They use their “spiders” or “crawler” and retrieve the information of the pages that they have found through the links, regularly.

|{{< figure src="/media/crawl.jpg">}}|
|-------------------------------------|
|Figure 4 : crawl base search engine (Ledford, 2009)|

They allow site owners to hide their information partially or totally from search engine by using “robots.txt” file protocols (Google, Google Basics, 2011).

#### Human-powered directories
There are open directories that allow site owners to add their website to them. These websites are sorted by humans and get the information, such as site title and description, from the submission form of the site proprietor. Users search through limited number of websites and the result matching with the submitted description.

#### Semantic search
It tries to understand the search query according to user intent and shows more relevant data by using search history.

#### Hybrid search engines
It’s a combination of semantic and crawl type search engines. This method tries to cope with limitation of current ontology based searches such as lack of logical coverage of content (Bhagde, Chapma, Ciravegna, Lanfranchi, & Petrelli, 2008).

### References
- Bhagde, R., Chapma, S., Ciravegna, F., Lanfranchi, V., & Petrelli, D. (2008). Hybrid Search: Effectively Combining Keywords and Semantic Searches. European Semantic Web Conference. White Rose Research.
- Google. (2011). Google Basics. Retrieved January 7, 2012, from Google: [[1]](http://www.google.com/support/webmasters/bin/answer.py?hl=en&answer=70897&rd=1)
- Ledford, J. (2009). Search Engine Optimization (2nd ed.). (M. B. Wakefield, Ed.) Indianapolis: Wiley.
- Zeller, T. (2006). A New Campaign Tactic: Manipulating Google Data. Retrieved January 8, 2012, from nytimes: [[2]](http://www.nytimes.com/2006/10/26/us/politics/26googlebomb.html)