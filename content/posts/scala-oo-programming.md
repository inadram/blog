---
 url : "scala-oo-programming"
 date : "2014-10-11"
 tags : 
  - "Programming"
  - "scala"
 title : "Scala OO programming by example"
---

Scala both support OO and functional programming. Scala OO support builds on Java but with below differences:

- No public class , so you can have more than one public class in a single file
- No file naming convention
- No static member but you can use companion objects as your static objects
- different way to declare constructors and interfaces

### Define class

```
class someClass{
  private var somePrivateVariable =0
  def increment(){ somePrivateVariable+=1}
  def balance() = somePrivateVariable
  var someFields =0
}
```

#### Call class

```
var some = new someClass()
some.balance() // 0
some.increment()
some.balance() //1
```

### Setters and Getters
The compiler generate getters and setters automatically for public fields.

setting value into field

```
some.someFields_=(10)
```

getting value from a field

```
some.someFields
```

you can overriding them by redefining them in class level.

### Constructor
the primary constructor is define in the input arguments of class

```
class Person(val name:String,age:Int){
  def getDetails ={ details}
  private val details = name +" is "+ age +" years old"
}
```

Scala will provide immutable public getter for the fields that define by val.

so in this example “name” will be accessible from outside while “age” is private

```
val person = new Person("Amir",28)
person.name // output: Amir
```

Anything is not in def then its part of primary constructor and will be executed

```
person.getDetails // output: Amir is 28 years old
```

#### Secondary constructor
you can define other constructors by adding “this” as a method to class

```
class Person(val name:String,age:Int){
  def getDetails ={ details}
  private val details = name +" is "+ age +" years old"
  def this(name:String) = this(name,0)
}
```

it will create an override for constructor and allow you to create object from the class by new signature.

```
val noAgePerson = new Person("Rima")
noAgePerson.getDetails //output: Rima is 0 years old
```

### Companion object
it is singleton and act as a static object for class. A class and companion access to each other member and should be in a same source file.

```
class person {}
object person{}
```

use object when you sure you only need one instance of them in whole project

```
object singletonThing{}
```  

#### Companion object as factories
you can use companion objects as factory to create instances of your class

```
class person(name:String,age:Int,sex:String) {
  def detail()={
    name +" is "+age+" old "+sex+" person "
  }
}
object person{
  def apply(name:String)={new person(name,0,"unknown")}
  def apply(name:String,age:Int)={new person(name,age,"unknown")}
  def apply(name:String,age:Int,sex:String)={new person(name,age,sex)}
}
```

you can using it as below

```
var firstPerson = person("Amir",10,"male")
firstPerson.detail() //output: Amir is 10 old male person

firstPerson = person("Amir",10)
firstPerson.detail() //output: Amir is 10 old unknown person

firstPerson = person("Amir")
firstPerson.detail() //output: Amir is 0 old unknown person
```

#### accessing private member

```
class person(val name:String) {
 def location = person.city
}

object person{
  private val city = "Manchester"
}
```

using it as below

```
val firstPerson = new person("amir")
firstPerson.location
```

### val vs def
“def” has lazy evaluation and it evaluate on execution while “val” is evaluate when it define.

```
val dtVal = new Date() //output: Tue Nov 11 20:47:26 GMT 2014
def dtDef = new Date() //output:

dtVal //output: Tue Nov 11 20:47:26 GMT 2014
dtDef //output: Tue Nov 11 20:47:26 GMT 2014
```
### Exception Handling
it has “Nothing” type and we use it with try/catch/finally

```
import java.io.IOException

try{
  1/0
}catch {

  case ex:IOException =>
  println("some IO exception")
  case ex:ArithmeticException=>
  println("some Arithmetic exception")
  case ex:Exception=>
  println("some exception")

}
finally {
    println("finally done !")
}
```
it will match with the first matching exception, so the out put will be as below

```
some Arithmetic exception
finally done !
```