---
 url : "head-tags-in-seo"
 date : "2013-01-11"
 tags : 
  - "SEO"
 title : "Head tags in SEO"
 description: "289714161456390144"
---
### Head tags
Webmasters can use concise phrases when describing the content of a page via multiple HTML heading size tags such as `“<h1>”`, `“<h2>”` and `“<h3>”`. These are important to inform the search engine about the hierarchical structure of their website and their important texts. Heading tags are an important component of the website which help visitors to more easily navigate a website .They are visual signs to make visitors pay more attention to the content which underneath the heading text could be useful for them.

|{{<figure src="/media/13.jpg">}}|
|---|
|Figure 19: Heading tags in HTML (Google, 2010)|

Heading tags could for example act as outline points for a large paper, they would help both visitors and Googlebots to understand about the start and end points of each page section which may not be obvious (Falls, Goradia, & Perez, 2010) (Google, Search Engine Optimization starter guide, 2010).While styling text so it appears larger might achieve the same visual presentation, it does not provide the same semantic meaning to the search engine that an `<h1>` tag does (Falls, Goradia, & Perez, 2010).  

The most common issue in using heading is that webmasters prefer to use `<h2>` or `<h3>` rather than `<h1>` to have more fancy pages, while `<h1>` is the most important tag which represent the main focus of the page to the Googlebots (Douvas & Lee, 2010) .  

At the same time, although using larger font could have the similar visual presentation, it does not carry the same meaning to Googlebots that heading tags do (Falls, Goradia, & Perez, 2010). In addition, these semantic markup signs guide Googlebots to understand the priorities of the content ( Szymanski, Far, & Naumann, Sharing advice from our London site clinic, 2011) while the order of using `<h1>` and `<h2>` tags is not of that much importance, using them in a random order will not make any difference (GoogleWebmasterHelp, Does the ordering of heading tags matter?, 2009).  


### Referencing
- *Google. (2010). Technology overview. Retrieved January 7, 2012, from Google: [[link]](http://www.google.com/about/corporate/company/tech.html)*
- *Falls, B., Goradia, A., & Perez, C. (2010). Google’s SEO Report Card. Retrieved January 7, 2012, from Google Webmaster Central: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/google-seo-report-card.pdf)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Douvas, A., & Lee, J. (2010). Holiday source code housekeeping: Website clinic for non-profits. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/12/holiday-source-code-housekeeping.html)*
- *Szymanski, K. (2010). Quality links to your site. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/06/quality-links-to-your-site.html)*
- *GoogleWebmasterHelp. (2009). Does the ordering of heading tags matter? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=iR5itZlq8sk)*