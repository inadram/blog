---
 url : "retweetline-com-is-now-live"
 date : "2012-10-23"
 tags : 
  - "RetweetLine" 
 title : "RetweetLine.com is now Live !"
 description: "260727176620552194"
---
Around 2 weeks ago I came with the idea of [RetweetLine.com](http://retweetline.com/), which aims to help twitter users by offering different services such as :

twitter analyser to digging an account
twitter bot that tweeting on their behalf
advertising their tweets by getting retweets


The first feature of website is now live ! and you can [try it for free](http://www.retweetline.com/AnalyseTwitter) ! with your twitter account .

{{< figure src="/media/analyse.jpg">}} 

I tried to use LEAN strategy to deploy it. in other words try to get it live as soon as possible and adding more future to each product after getting feedback from the users.

The project developed with MVC 4 with proper TDD . I tried  my best to let the customer access the service with minimum clicks and in easy steps.