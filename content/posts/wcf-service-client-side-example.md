---
 url : "wcf-service-client-side-example"
 date : "2013-01-21"
 tags : 
  - "Programming"
  - "WCF"
 title : "WCF service client side example"
 description: "293649996866260992"
---
### What is client side ?
Basically, the client gets the service endpoints and contracts through the WSDL file and then make a channel to send the message to the service.

### How to consume a service ?
You can use the svcUtil.exe tool to make the required .cs and ,config files and then adding them to the project or just add the web reference  and add your service meta address your client project .

#### Chanel factory
The next step is to make a Chanel to communicate with the server.There are two ways to invoke the service from client side.

#### IChannelFactory
IChannelFactory get the service contract with our desire binding way. Then making a channel with the EndPointAddress of our binding which can be found in the app. Config file. Within the channel object we can call the service methods.

```
IChannelFactory<IMachine> factory = new ChannelFactory<IMachine>(new BasicHttpBinding());
IMachine channel = factory.CreateChannel(new EndpointAddress("http://localhost:8733/ServiceMachine/basic"));
	try {
		string machineName = channel.GetMachineName(new MachineDTO {MachineID = "1", MachineName = "test"});
		Console.WriteLine(machineName);
		Console.ReadLine();
		((IClientChannel) channel).Close();
	}
	catch (FaultException faultException) {
		((IClientChannel) channel).Abort();
		Console.WriteLine(faultException.Message);
	}
	catch (CommunicationException communicationException) {
		((IClientChannel) channel).Abort();
		Console.WriteLine(communicationException.Message);
	}
	       
```

If something goes wrong while invoking the service , it is possible to catch the exception and abort the connection.

In order to close the channel we have to cast it to IChannelFactory or using IMachineChannel interface which is generated when we importing the wsdl file into our client .

```
IChannelFactory<IMachineChannel> factory = new ChannelFactory<IMachineChannel>(new BasicHttpBinding());
	IMachineChannel channel = factory.CreateChannel(new EndpointAddress("http://localhost:8733/ServiceMachine/basic"));
	try {
		string machineName = channel.GetMachineName(new MachineDTO {MachineID = "1", MachineName = "test"});
		Console.WriteLine(machineName);
		Console.ReadLine();
		channel.Close();
	}
	catch (FaultException faultException) {
		channel.Abort();
		Console.WriteLine(faultException.Message);
	}
	catch (CommunicationException communicationException) {
		channel.Abort();
		Console.WriteLine(communicationException.Message);
	}
The IMachineChannel derived from IClientChannel and IMachine therefore we have access to both class functionality.
```
#### Using Proxy class
It is also possible to establish the client channel with the special proxy class that import tool generated for us. The name of the class made up by removing the “I” and adding the “Client” to the end of it .

```
var channel = new MachineClient("BasicHttpBinding_IMachine");
```

it accept our binding name as an input data and generate a channel for us. The name of the binding can be find in the client app.config file.

### Download
Feel free to download the source code of this wcf example from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF/WCFService).
