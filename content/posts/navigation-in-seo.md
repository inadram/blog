---
 url : "navigation-in-seo"
 date : "2012-12-18"
 tags : 
  - "SEO"
 title : "Navigation in SEO"
 description: "280963197568159744"
---

Search engine consider “navigation” as an important factor in their algorithm as it can help Google to find out important content of each website as well as it can guide visitors to find their desirable content quickly. Google suggest webmasters to plan out their navigation based on their homepage wisely with either “navigational menu”, “text-based links” or even “user-viewable site map” (Lee & Douvas, 2010) which has a natural text-based flowing hierarchy to enable visitors to navigate through their website even by removing part of the URL without encountering “not found 404” error pages (Google, Search Engine Optimization starter guide, 2010).

|{{< figure src="/media/1.jpg">}}|
|---| 
|Figure 13: the directory structure of sample website (Google, 2010)|

Having a text-based clickable navigation links which contain descriptive anchor texts is a useful signal that helps both visitors and search engines to understand the content of the page and it can help Google to return more relevant results for users (Ohye, Importance of link architecture, 2008).

### References  

- *Lee, J., & Douvas, A. (2010). Ring in the new year with accessible content: Website clinic for non-profits. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://googlewebmastercentral.blogspot.com/2010/12/ring-in-new-year-with-accessible.html)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Google. (2010). Technology overview. Retrieved January 7, 2012, from Google: [[link]](http://www.google.com/about/corporate/company/tech.html)*
- *Ohye, M. (2008). Importance of link architecture. Retrieved January 7, 2012, from google webmaster central: [[link]](http://googlewebmastercentral.blogspot.com/2008/10/importance-of-link-architecture.html)*