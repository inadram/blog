---
 url : "hosting-wcf-service-example"
 date : "2013-01-16"
 tags : 
  - "Programming"
  - "WCF"
 title : "Hosting WCF service example"
 description: "291678475096559616"
---

WCF service can be hosted in two ways.

### 1. Self-hosting
You can use the ServiceHost Class to host any .net 3.x application inside your project.
In this case you can make a console application and add the reference of the WCF service and also System.ServiceModel to your project.

```
var serviceHost = new ServiceHost(typeof(Machine));
```

In the next step you can add endpoints to your service host instance  

```
serviceHost.AddServiceEndpoint(typeof(IMachine), new BasicHttpBinding(),
"http://localhost:8733/ServiceMachine/basic");

serviceHost.AddServiceEndpoint(typeof(IMachine), new WSHttpBinding(), 
"http://localhost:8733/ServiceMachine/ws");

serviceHost.AddServiceEndpoint(typeof(IMachine), new NetTcpBinding(), 
"net.tcp://localhost:8731/ServiceMachine");
```

You pass your service contract interface type along with your desire protocols. Each end point should have a unique URI address.

Each binding is suited for a specific type of communication. For instance, WebHttpBinding support REST-style communication while BasicHttpBinding is for SOAP.

#### Binding Configuration
Also it is possible to tailor a bindings to meet our needs.

```
var basicHttpBinding=new BasicHttpBinding();
basicHttpBinding.Security.Mode=BasicHttpSecurityMode.Transport;
basicHttpBinding.Security.Transport.ClientCredentialType=HttpClientCredentialType.Basic;

serviceHost.AddServiceEndpoint(typeof(IMachine), basicHttpBinding,
"http://localhost:8733/ServiceMachine/basic");
```

#### Behavior configuration
It is also possible to add behavior to the service . For instance in order to set the meta exchange behavior we can come up with something like this.

```
var serviceMetadataBehavior=new ServiceMetadataBehavior {
	HttpGetEnabled = true,
	HttpGetUrl = new Uri("http://localhost:8733/ServiceMachine/meta")
};
serviceHost.Description.Behaviors.Add(serviceMetadataBehavior);
```

Then you can open the service or abort it if you encounter to any exceptions.

```
try {
	serviceHost.Open();
	Console.ReadLine();
	serviceHost.Close();
}
catch (Exception exception) {
	Console.Write(exception);
	serviceHost.Abort();
}
```
### 2. Managed hosting
It is also possible to host your WCF service in IIS/ASP.net application without concern about ServiceHost class and let ASP.net take care about our service. It is possible to use SvcConfigEditor.exe in order to maintain your config file.

```
<services>
	<service name="WCFService.Machine">
		<clear />
		<host>
			<baseAddresses>
			<add baseAddress="http://localhost:8733/ServiceMachine/" />
			</baseAddresses>
		</host>
		<endpoint address="basic" binding="basicHttpBinding" contract="WCFService.IMachine" />
			<endpoint address="ws" binding="wsHttpBinding" contract="WCFService.IMachine" />
		<endpoint address="net.tcp://localhost:8732/ServiceMachine/" binding="netTcpBinding contract="WCFService.IMachine" />
		</service>
</services>
```
The logic behind it, is the same but the syntax is different. The base address is your main address and the address attribute in the endpoints represent the relative address.

#### Binding Configuration
Tailoring the binding we can use the binding configuration after the close </services> tag as below.

```
<bindings>
	<basicHttpBinding>
		<binding name="customBindings">
			<security mode="Transport">
				<transport clientCredentialType="Basic"/>
			</security>
		</binding>
	</basicHttpBinding>
</bindings>
```
Then map it to your desire basicHttpBindings

```
<endpoint address="basic" binding="basicHttpBinding" contract="WCFService.IMachine" bindingConfiguration="customBindings" />
```
#### Configure behavior
Similarly to the binding you should define the serviceBehavior tag .

```
<serviceBehaviors>
	<behavior name="meta">
		<serviceMetadata httpGetEnabled="true" />
		<serviceDebug includeExceptionDetailInFaults="false" />
	</behavior>
</serviceBehaviors>
```
And then set it in behaviorConfiguration attribute.

```
<service behaviorConfiguration="meta" name="WCFService.Machine">
```

### Download
Feel free to download the source code of this wcf service example from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF).
