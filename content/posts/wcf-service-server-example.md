---
 url : "wcf-service-server-example"
 date : "2013-01-15"
 tags : 
  - "Programming"
  - "WCF"
 title : "WCF service server side example"
 description: "291275587320950784"
---
### What is WCF ?
Windows Communication Foundation is  service application that help us to send messages between connected systems  by  various endpoints.

WCF support both SOAP and Restful design architectures and could transport various types of message format on different protocols while previous Microsoft services such as ASMX and WSE did not. For instance we can send XML messages on  HTTP protocol when our concerns about interoperability or binary message through TCP when we are looking to increase the performance.

It can communicate with different OS in XML base pockets while other old communication frameworks such as DCOM , .Net Remoting and MSMQ restricted us to windows OS [ [more info](http://msdn.microsoft.com/en-us/library/ms731082.aspx) ] .

### Example

#### First step : defining message structure
In this step we map the message format to ours .NET object . Basically we serialize the object with System.Runtime.Serialization.DataContract and DataMember attributes as we use Typed messaging format.

```
[DataContract]
public  class MachineDetails {
	[DataMember]
	public string Name;

	[DataMember]
	private int _id;

	public bool Original;
}
```    

Although _id is private, it’s still included in the message. At the same point “Original” in not included in the message.

#### Second step: defining service contract
Its grouping the operation and define the signature pattern the service can invoke. All the required method should align with “OperaionContrcat” otherwise it won’t represent to your external client.

“Add” method is a one way operation as we do not look for a response for this method.

```
[ServiceContract]
interface IMachine {
	[OperationContract(IsOneWay = true)]
	void Add();

	[OperationContract]
	string GetMachineName(Machine machine);
}
 ```
#### Third step :implementing service
To implement the service , we should ask our class to implement from the interface and then we can put our business logic in it .

Also you can set up your class behavior in it.

```
[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,ConcurrencyMode = ConcurrencyMode.Multiple) ]
class Machine:IMachine {
	public void Add(MachineDTO machineDTO) {
	    var db=new DummyDatabase();
	    db.submit(machineDTO);
	}
	public string GetMachineName(MachineDTO machineDTO) {
	    return string.Format("the machine name is {0}",machineDTO.MachineName);
	}
}
```    

InstanceContextMode :

Percall means every incoming message gets new instantiated
Single means there is a singleton instance that handles all incoming messages
PerSession means each client gets its own instance of the service .
ConcurrencyMode:

Single means one thread at a time is allowed
Multiple means multiple thread is allowed
Re-entrant is same to single while it will allow another incoming thread when there is a pending outbound call

#### Last step : Hosting service
You can setup hosting of the service within your code or in the config file.In this example we update the service address and contract in the app config file.

```
<service name="WCFService.Machine">
contract="WCFService.IMachine"
```

#### Running the application
After running the application you can invoke the methods.

### Download
Feel free to to download the source code of the project from my [GitHub](https://github.com/inadram/Code-Sample/tree/master/WCF/WCFService).