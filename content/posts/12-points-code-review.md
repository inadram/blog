---
 url : "12-points-code-review"
 date : "2014-06-09"
 tags : 
  - "review"
  - "Methodology"
 title : "12 good points in code review"
 description: "477002788824547328"
---

### What is review?
It is a visual inspection of (typically) source code by someone other than the author, in order to identify defects with the intention of rectifying those defects.  

### Why we need review ?
find issues and fix them early in the software life cycle. early fixes helps us to save time and cost.
it will improve code quality , code clarity , verification and validation of your product. it will help you to understand your code better as well as sharing your knowledge to others. it will improve collaboration and communication between team member.
it will bring consistence to your code base and familiar new team member with the best practices.
You will have more confidence in your code by involving other people with different expertise in your code and agree with your approach.

### Some good points to consider
#### Small reviews

 {{<figure src="/media/review.jpg">}}

- Try to keep the number of code to review as less as possible.  
- The number of code that you reviewing should not go beyond 400.  
- According to the cisco study the number of defects found drops off as the number of lines reviewed increases.  
- So you avoid large code changes (break it down to  smaller parts) and have a proper time in reviewing the code.  


#### Short reviews (re time doing review)
- your review time should not go beyond 90 minutes.  
- try to slow than and be more effective.  

#### Understand the requirements (functional and non-functional) before review
- you should understand all functional and none functional requirement before starting to review the code.  
- consider both typical and exceptional path  

#### Normal part of working day
- treat code review as a “big thing”. You can add it as a column to your agile board.  
- do it as a part of normal working hours and do not leave it to the friday afternoon as it might be drop off soon .  

#### Review Structure
- Avoid large formal reviews and keep those involve in the review small (pair review or three way review)  
- select your reviewer base on knowledge experience and availability.  
- Try to engage with your reviewer and do not be passive  
- Top down approach helps you to understand and consider code structure and some wide NFRs of the system.  
- Bottom up approach helps you to consider code in line level and check some specifics analysis e.g. system performance.  
- Consider well known issues such as Top 25 dangerous programming errors or `OWASP` top ten security issues  

#### Use a CheckList
- have a check list of common mistakes to check the code against them but treat them as an aid not a tick list. e.g. code smell  

#### Foster positive behaviour around reviews
- management and team must ensure that , finding a defect in review is a good thing.  
- avoid blame game ! review provide learning, growth and communication.  
- Author and reviewer should both try to find defects.  

#### Establish Goals and Metrics Around Reviews
- Have a goal for your review e.g. find bug and potential problem, ensure consistence style, etc  
- How to measure effectiveness of code review? how many issue identified during review, how long a review took.  

#### Authors annotate source code before reviews
- document the changes before review  
- this is not commenting , its more about design and decision that you made for the code structure  
- it can helps you to correct your self  before review  
- you can use JIRA or reviewboard for commenting and discussing about the code  
- You can use SVN or Git for commit comments  
- Visualise your changes to see how your structure is changing after  

#### Ego Effect – everyone is reviewed, everyone reviews
- everybody review and everybody get reviewed, there is no special people who do not need to be reviewed  
- we all make mistake  
- try to critique the author with “bad news sandwiches” method !  

#### Ownership – developer and reviewer proud of code
- the result of code review is owned by both author and reviewer  
- any good feature or defect that go through live, both reviewer and author are responsible  

#### Record / log review notes and conclusions, and verify issues fixed
- we have bad memory so noted down all the defect and decision that you made through your review  
- make sure your changes during the review fix the issue and do not break any other current feature!  

### Read more
[Best Kept Secrets of Peer Code Review](http://smartbear.com/resources/whitepapers/best-kept-secrets-of-peer-code-review/)