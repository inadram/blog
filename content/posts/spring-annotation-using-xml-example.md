---
 url : "spring-annotation-using-xml-example"
 date : "2014-05-12"
 tags : 
  - "Programming"
  - "Java"
  - "Spring"
 title : "Spring annotation using XML example"
 description: "465770134452187136"
---

### What is it ?
It is another way to wire up beans in java application.

### How is it works?
#### setup applicationContext.xml
similar to Spring XML configuration, we need to create our main xml config file.

> applicationContext.xml

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
 
</beans>
```

#### Component Scanner
We need to edit our applicationContext.xml file and specify the context of it.

> context of xml file

```
<context:annotation-config/>
```

Also we need to let it know, what is our base package is. Therefore it can start scanning the files and looking for the annotation from that location and drill down to it.

> base package

```
<context:component-scan base-package="com.inadram" />
```

### Stereotype Annotation
It help us to find component or beans inside the application.

#### Types of stereotype annotation
Semantically all of them are same and extend from **@component** but they help us to have better understanding of our code. 

**@component** It use for regular component/beans. 

**@service** It is not a web service, actually it point to our business logic. 

**@Repository** it use for our data access layer.  


### How to use annotations ?
We need to put the annotation at the top of the class. We can use either **@Repository** or **@component** for our repository classes. We can name it anything but here we use the same name as interface but with lowercase convention.

> repository class

```java
@Repository("customerRepository")
public class hibernateRepositoryImplementation implements CustomerRepository {
 
    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<Customer>();
 
        Customer customer = new Customer();
        customer.setFirstName("Amir");
        customer.setLastName("Mardani");
        customers.add(customer);
        return customers;
    }
}
```

We are doing same thing with our service class.

> service class

```java
@Service("customerService")
public class CustomerServiceImplementation implements CustomerService {
 
    private CustomerRepository customerRepository = new hibernateRepositoryImplementation();
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
 
}
```

### Auto Wiring
We can auto wire in 3 different places.

#### Member variable
We place the auto wiring annotation on the private variable and it use reflection to set it.

> member variable

```java
@Service("customerService")
public class CustomerServiceImplementation implements CustomerService {
 
    @Autowired
    private CustomerRepository customerRepository;
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
 
}
```

It will use the implementation of the `CustomerRepository` i.e. `hibernateRepositoryImplementation` instance and inject it into our `CustomerSeviceImplementation` class and set it to our private `customerRepository`.

#### Constructor
We can place the auto wiring annotation on the constructor.

> constructor auto wired

```java
@Service("customerService")
public class CustomerServiceImplementation implements CustomerService {
 
    private CustomerRepository customerRepository;
 
    @Autowired
    public CustomerServiceImplementation(CustomerRepository customerRepository){
        this.customerRepository= customerRepository;
    }
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
 
}
```
#### Setter
We can place the auto wiring annotation on the setter and it will call it to set it.

> setter injection

```java
@Service("customerService")
public class CustomerServiceImplementation implements CustomerService {
 
    private CustomerRepository customerRepository;
 
    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
 
}
```
### Using it
Similar to [XML approach](http://blog.inadram.com/spring-example/) , We need to read the applicationContextXml file and find the bean to calling it .

> application context

```java 
public class Application {
    public static void main(String args[]) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        CustomerService customerService = applicationContext.getBean("customerService", CustomerService.class);
        System.out.println(customerService.findAll().get(0).getFirstName());
    }
}
```
### Download
Feel free to download the full source code of this example from my [github](https://github.com/inadram/Code-Sample/tree/master/Java/Spring/spring-annotation-example).
