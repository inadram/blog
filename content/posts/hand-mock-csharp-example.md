---
 url : "hand-mock-csharp-example"
 date : "2013-01-09"
 tags : 
  - "Test" 
  - "example"
  - "Mock"
  - "unit test"
  - "c#"
 title : "C# example of hand mock test"
 description: "289132433117421571"
---
### What is hand mock ?
You faking the dependency by implementing it in your test class and inject it to your production code .

### Example
#### Production code
Imagine you have Machine class which have couple of dependency like below

> Machin classC#

```
public class Machine {
        private readonly IPrinter _printer;
        private readonly IConfigureSystem _systemConfiguration;

        public Machine(IPrinter printer, IConfigureSystem systemConfiguration) {
            _printer = printer;
            _systemConfiguration = systemConfiguration;
        }
}
```

Then there is a Action method inside it , which check the type of passing action and do the appropriate action, like below

> Action method 

```
 public void Action(string message, string Type) {

            if (_systemConfiguration.CheckIfPrint(Type)) {
                 _printer.Print(message);
            }
            else {
                //do something else
            }
        }
```
#### Test Code
Now we want to test that if the Print method inside the Action method Called or not .

> test classC#  

```
[TestFixture]
    class TestLogging
    {
        public void Given_Logging_was_called()
        {
           var machine = new Machine(null, null);
            machine.Action("blah blah blah ....", "Print");
            Assert.That(printer.PrintWasCalled());
        }
    }
```    
#### inject dependency
To pass the dependency , we have to make mock classes in the test side and ask them to implement from those interfaces. we can add boolean flag to the Print method in our mock class to find out if it get called or not .

Along with that , we can ask the checkIfPrint() method to always return true , in order to enter to the if statement as we dont want to test this method right by now.

> implement Interfaces that want to injectC#

```
public class _printer : IPrinter
    {
        private bool isCalled = false;
        public string Print(string messageToScrub)
        {
            isCalled = true;
            return string.Empty;
        }
 
        public bool PrintWasCalled()
        {
            return isCalled;
        }
    }
 
    public class _configure : IConfigureSystem
    {
        public bool CheckIfPrint(string logLevel)
        {
            return true;
        }
    }
 
    [TestFixture]
    class TestLogging
    {
        public void Given_Logging_was_called()
        {
            var printer = new _printer();
            var configure = new _configure();
            var machine = new Machine(printer, configure);
            machine.Action("blah blah blah ....", "Print");
            Assert.That(printer.PrintWasCalled());
        }
    }
```
    
That’s it ! now if the print method get called, the flag will be true otherwise it will be failed.

### hand mocks Vs mock framework
#### Benefits
You have more control on your code
You do not make your code so complex as mocking it will be harder. that push you to write more SOLID code.
#### Draw backs
Mocking object make your code complex and large
if the interface change , then you are in trouble ! as you have to implement them again in your tests.
Tied up your tests to your business logic and make it hard to change the structure or business rules of the project.
Take more time to write tests
### Download
Feel free to download source code of this example from my [GitHub](https://github.com/inadram/TestingApproches/tree/master/HandMock) 