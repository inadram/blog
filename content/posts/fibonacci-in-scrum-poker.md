---
 url : "fibonacci-in-scrum-poker"
 date : "2012-08-16"
 tags : 
  - "Agile" 
  - "Methodology"
  - "Scrum"
  - "Scrum Poker"
 title : "Why use Fibonacci in Scrum poker ?"
 description: "236048755441414144"
---

### what is Scrum poker?

[Scrum poker](http://en.wikipedia.org/wiki/Planning_poker) help a [scrum](http://www.youtube.com/watch?v=WxiuE-1ujCM&feature=youtu.be) team to underhand the complexity of each task and estimate the time that they need to do it.

### what are the methods of estimation ?

- Numeric sizing (1 to 10)
- T-shirt sizing (XS to XXXL)
- Fibonacci sequence (1,2,3,5 to 89, Coffee Time to think more about it !)
- even dog breeds sizes ! (Chihuahua  to Great Dane)
- [more details](http://scrummethodology.com/scrum-effort-estimation-and-story-points/)

### Why Fibonacci is better ?

- the first and the most important reason is ” Its cool ! “
- as the number increase the level of uncertainly increase and that remind you to break down a task [more detail](http://www.jiraplanningpoker.com/agile-methodology-story-points-estimation-in-planning-poker-elegant-plugin-for-scrum-masters.html)
- larger gaps between people estimation help to understand the level of uncertainly [more detail](http://www.scrum-institute.org/Effort_Estimations_Planning_Poker.php)

### A quick example

1. what is the size of your phone in centimeter ? [numeric :1] [T-shirt :XS] [Fibonacci :1]
2. what is the size of your desk in centimeter ? [numeric :2]  [T-shirt :S] [Fibonacci :2]
3. what is the distance between your desk to front door in centimeter ?[numeric :4]  [T-shirt :M]  [Fibonacci :13]
4. what is the distance between your desk and your house door in centimeter ? [T-shirt :L] [Fibonacci :21]
5. what is the distance between your house to Pizza tower ?! [numeric :1000] [T-shirt :XXXL][Fibonacci : Coffee !]

### conclusion 

basically you should revise when your level of uncertainly increase or you are not sure about the complexity of the task  and Fibonacci give you a better feeling to choose a right task to break down .

In my opinion the best way of Scrum poker is use [Chris Sims](http://blog.technicalmanagementinstitute.com/2009/06/story-sizing-a-better-start-than-planning-poker.html) method and order the task from easy to hard and then try to assign complexity number with Fibonacci algorithm.

