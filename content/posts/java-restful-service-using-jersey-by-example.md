---
 url : "java-restful-service-using-jersey-by-example"
 date : "2014-06-20"
 tags : 
  - "Programming"
  - "Java"
  - "rest"
 title : "Java RESTful service using jersey by example"
 description: "479907150282493953"
---
### What is jersey ?
Jersey is an implementation of JAX-RS specification or in other words its java implementation of REST. Its use as  service with REST pattern.

### What is REST?
REST build on the HTTP verbs  such as POST, GET, PUT, DELETE. It can transfer data in a plain text, XML, JSON or binary octet such as pdf and images content.

#### RESTFul urls
urls are heart of restful services and we always concentrate around nouns rather than verbs. verbs are using to send the command to the service.

for example, example.com/hotel/reservation/ should return all the hotel reservation and to add,update, delete or retrieve a specific reservation we should use example.com/hotel/reservation/101 url.

### What is Tomcat
Tomcat is a common server for developing java services. Tomcat is a lightweight, simple and fast web container that accept a war file and run the application.

#### Download Tomcat
Download the stable core zip source code of it from apache website and put it in your developing folder. You can add the Tomcat server in Intellij by going to the IDE setting/ application servers and add server and map it to the source code which you have just downloaded.

### Download Jersey using Maven
Jersey recommend to download it via Maven as it will setup all the dependencies and configuration for us.

> maven command

```
mvn archetype:generate -DarchetypeGroupId=org.glassfish.jersey.archetypes -DarchetypeArtifactId=jersey-quickstart-webapp -DarchetypeVersion=2.2
```

After running the above command in the console, we should choose the groupId and artifactId. make sure you are not using special character such as dash in your group id name.

### setting names

```
'groupId': : jerseyGroupId
'artifactId': : jersey-artifact
```

After download completed, you need to import the package as Maven to your project.

### What does it consist?
it have standard common directories e.g src/main/java and src/main/resources

**java**
all of our java source code will go under this folder.

**resources**
all the property or xml files are under this folder

**pom.xml**
it has all the dependencies of our project.

**web.xml**
it contain the servlet configuration such as url pattern and our base folder.

### Creating service
to run a service we need to specify at least three things.

> my resource service

```
@Path("myresource")
public class MyResource {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
}
```

**Path to the service
@Path**

it need to know the path to access the service.

**the request method
@Get**

It need to know the request mode which can be get, post, put or delete.

**Response from the service
@Produces(MediaType.TEXT_PLAIN)**

It need to know the response type which can be anything from plain text to binary file.

### Run the tomcat
Finally you need to add your project as war file to the tomcat. If you are using Intellij you can add it by adding build war file through server configuration.

By running the service and navigating to the url you will get the expected message . i.e. `http://localhost:8080/webapi/myresource`

### Download
Feel free to download the full source code of this example from my [github](https://github.com/inadram/Code-Sample/tree/master/Java/Jersey/jersey-artifact).
