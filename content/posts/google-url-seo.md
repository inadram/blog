---
 url : "google-url-seo"
 date : "2012-12-11"
 tags : 
  - "SEO"
 title : "What Google says about importance of URL in SEO"
 description: "280963197568159744"
---

URLs should be simple-to-understand and convey the information of their contents easier.  

Google consider the URL of the pages as a signal for ranking websites and their images (GoogleWebmasterHelp, Does Google consider the URL of an image?, 2009) and ask webmasters to have a descriptive URL for the categories and filenames to not only have a more organised website that can help visitors to know where they are at the website but also it could help search engines to crawl their website better. On the other hand, although having “friendlier” URLs can help those who want to link to content, bear in mind that rewriting the dynamic URLs to a more friendly version is a tricky action which could cause crawling issues if being done incorrectly (Google, Search Engine Optimization starter guide, 2010) (Google, Google-friendly sites, 2011).  

| {{< figure src="/media/151.jpg" >}} |
|----| 
|Figure 12: URLs in Google result page (Google, 2010)|

Furthermore, some of the users prefer to use URL of a page as an anchor text, therefore URLs which contain relevant keywords can help both users and search engine to know more about the content of the page (Google, Google-friendly sites, 2011) .  

Understanding that relevant keywords of a URL is displayed in search result in a bold state, therefore more bold letters could lead to more clicks (Google, URL structure, 2011) .  

Google suggest webmasters to use punctuation in their URLs and ideally put hyphens (-) rather than underscores (_) between words (GoogleWebmasterHelp, Underscores vs. dashes in URLs, 2011).  

Similarly, Google strictly ask webmaster to avoid removing URLs extension e.g. “.html” (GoogleWebmasterHelp, Should I strip file extensions from my URLs?, 2009) and also do not use “&id” for anything other than session id in the URL as Google usually treat this parameter as session id and do not index it (CUTTS, 2006).  

Similarly, Google recommend webmasters to avoid using lengthy URLs which contains unnecessary parameter .Using URLs that contain keywords excessively or having different URLs for one page could have negative impact on a website ranking (Google, Search Engine Optimization starter guide, 2010).  


### References
- *CUTTS, M. (2006). Guest post: Vanessa Fox on Organic Site Review session. Retrieved January 7, 2012, from mattcutts: [[link]](http://www.mattcutts.com/blog/guest-post-vanessa-fox-on-organic-site-review-session/)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Google. (2011). URL structure. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=76329)*
- *Google. (2011). Google-friendly sites. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=40349)*
- *GoogleWebmasterHelp. (2009). Should I strip file extensions from my URLs? Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?feature=player_profilepage&v=dSG6C33GwsE)*
- *GoogleWebmasterHelp. (2011). Underscores vs. dashes in URLs. Retrieved January 7, 2012, from Youtube: [[link]](http://www.youtube.com/watch?v=AQcSFsQyct8)*
- *GoogleWebmasterHelp. (2009). Does Google consider the URL of an image? Retrieved January 7, 2012, from youtube: [[link]](http://www.youtube.com/watch?v=h2SWuUobbr0)*