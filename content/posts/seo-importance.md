---
 url : "seo-importance"
 date : "2012-11-21"
 tags : 
  - "SEO" 
 title : "Importance of SEO"
 description: "271365141134721024"
---

Every month, more than eighteen billion web searches are performed on the internet (RESTON, 2011) and companies find it a good place to introduce their products to their potential customers due to its comparatively “lower cost”, “more focus” and “much straightforward” (SERVE, 2008) (Fron, 2005).
Alternatively, studies shows that searching is very popular between rich people (RESTON, 2011) (Enge, Spencer, Fishkin, & Stricchiola, 2010, p. 31) and in addition to this, according to the AOL, survey users has more intent to click on organic search result as compared to the pay per click advertising. As the visibility of links which are listed in the natural result is mostly double or more – up to 6 times – than one that are placed in the same position in paid links (Enge, Spencer, Fishkin, & Stricchiola, 2010, p. 169). Thus, foresighted business managers are looking for ways to improve their website accessibility probability in SERP search engines result pages by using search engine optimisation methods (SEO).

### what SEO stands for ?
SEO which stand for “Search Engine Optimizer” or “Search Engine Optimisation” (Google, Search Engine Optimization (SEO), 2011) which is a set of small modification to part of website that can help to get more hits from search engines (Jerkovic, 2010, p. 1) which are often categorised by the below detailed sections (Google, Webmaster Guidelines, 2011).

- Design and content e.g. sitemap, number of links, useful content, etc.
- Technical development e.g. hosting, use of JavaScript, error pages.
- Quality guideline e.g. avoids duplicate content, doorway pages, etc.
- These changes could also damage the ranking of a website and consequently its reputation, if they are not properly utilised and positioned (Google, Search Engine Optimization (SEO), 2011).

#### References
- *RESTON, V. (2011). comScore Releases January 2011 U.S. Search Engine Rankings. Retrieved January 8, 2012, from comscore: [[1]](http://www.comscore.com/Press_Events/Press_Releases/2011/2/comScore_Releases_January_2011_U.S._Search_Engine_Rankings)*
- *SERVE, I. (2008). Advantages and Disadvantages of Internet Advertising. Retrieved January 7, 2012, from article alley: [[2]](http://ibserve.articlealley.com/advantages-and-disadvantages-of-internet-advertising-690918.html)*
- *Fron, C. (2005). Internet Advertising Advantages. Retrieved January 7, 2012, from Yahoo! Contributor Network: [[3]](http://voices.yahoo.com/internet-advertising-advantages-1497.html)*
- *Enge, E., Spencer, S., Fishkin, R. & Stricchiola, J.C. 2009, The art of SEO, O’Reilly Media, Inc.*
- *Murray, G. (2009). Seo Secrets (2nd ed.). Divine Write.*
- *Google. (2011). Search Engine Optimization (SEO). Retrieved January 7, 2012, from Google Webmaster Tools Help: [[4]](http://www.google.com/support/webmasters/bin/answer.py?answer=35291)*
- *Jerkovic, J.I. 2009, SEO warrior, O’Reilly Media.*