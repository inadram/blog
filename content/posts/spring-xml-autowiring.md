---
 url : "spring-xml-autowiring"
 date : "2014-05-08"
 tags : 
  - "Programming"
  - "Java"
  - "Spring"
 title : "Spring XML Autowiring"
 description: "464324084638031873"
---
### What is Autowiring ?
It allow us to automatically wiring interfaces to the write implementation and help us to reduce some manual coding.

### How it works?
To auto wire below service class, we can use different types of auto wiring

> customer service

```java 
public class CustomerServiceImplementation implements CustomerService {
	private CustomerRepository customerRepository;

	public CustomerServiceImplementation(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

}
```
### byType
it allow beans to auto wired if there is only one type of beans exist in the container.
it will raise an exception if it find more than one.
it will do nothing if it could not find any bean from that type in the container

#### How it works?

> applicationContext.xml

```
<bean name="customerService" class="com.inadram.service.CustomerServiceImplementation" autowire="byType"/>
```

```java
public class CustomerServiceImplementation implements CustomerService {
	private CustomerRepository customerRepository;

	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
```

It will find the concrete implementation of the CustomerRepository interface and pass it through the method that start with “set” keyword and has CustomerRepository argument type.

bear in mind you need to have your default constructor if you using with args constructors.

### byName
Autowired will looking for the same name in the xml configuration file.

#### How it works ?

> customer service

```java
public class CustomerServiceImplementation implements CustomerService {
	private CustomerRepository customerRepository;

	public void setCustomerRepo(CustomerRepository customerRepository) {
		System.out.println("hi");
		this.customerRepository = customerRepository;
	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
```
> xml config file

```
<bean name="customerRepo" class="com.inadram.repo.hibernateRepositoryImplementation"/>
<bean name="customerService" class="com.inadram.service.CustomerServiceImplementation" autowire="byName"/>
```

It will be looking for a method in CustomerServiceImplementation class that named “setCustomerRepo” to pass the concrete class that we have defined in our xml configuration i.e. customerRepo into it.

### constructor
It is similar to byType but it applies to constructor argument.

#### How it works ?

```java
public class CustomerServiceImplementation implements CustomerService {
	private CustomerRepository customerRepository;

	public CustomerServiceImplementation(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}
```

### constructor auto wiring

```
<bean name="customerService" class="com.inadram.service.CustomerServiceImplementation" autowire="constructor"/>
```

It will find the concrete implementation of the CustomerRepository interface and inject it into the CustomerServiceImplementation class.

### no
It specifies that we are not using auto wiring at all.

### Download
Feel free to download the full source code of this example from my [github](https://github.com/inadram/Code-Sample/tree/master/Java/Spring/spring-example).