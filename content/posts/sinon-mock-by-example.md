---
 url : "sinon-mock-by-example"
 date : "2014-03-24"
 tags : 
  - "Programming"
  - "JavaScript"
  - "sinon"
 title : "Sinon mock by example"
 description: "448189949418827776"
---

### What is sinon mock ?
Mocks are fake methods with pre programmed behaviour and expectation. it replacing the system under test object with a test specific object to verify that it has been used correctly or not.(xUnit test pattern)

### When I should use sinon mock ?
Mock should being used for method under the tests. you can used mock to check the unit under test get used as your expectation  or not. You should not use mock if you have more than one assertion in single test.

### Production Code
below is the production code that we tried to write some test cases around it to demo the mock functionalities.

> production code

```javascript
var sinonMock = {

	callMyMethod: function (number, firstArgument, secondArgument) {
		for (var i = 0; i < number; i++) {
			this.myMethod(firstArgument, secondArgument);
		}
	},

	myMethod: function () {
		//do something
	}
};
```

### Test Cases
below are the test cases for sinon mock.

#### Test if my method get called

> called once

```javascript
SinonMockTest.prototype.testThatMyMethodGetCalledOnce = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	theSinonMock.expects("myMethod").once();
	sinonMock.myMethod();
	theSinonMock.verify();
};
```
it would check if “myMethod” function get called one time or not.
firstly you need to create a handler for mocked sandbox and then write your expectation with it. it will be fail on the verify bit if it is not meet your expectation.

> called twice

```javascript
SinonMockTest.prototype.testThatMyMethodGetCalledTwice = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	theSinonMock.expects("myMethod").twice();
	sinonMock.callMyMethod(2);
	theSinonMock.verify();
};
```

it check if “myMethod” function get called twice or not.

> called three times

```javascript
SinonMockTest.prototype.testThatMyMethodGetCalledThreeTimes = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	theSinonMock.expects("myMethod").thrice();
	sinonMock.callMyMethod(3);
	theSinonMock.verify();
};
```

it check if “myMethod” function get called three times or not.

#### Test if my method get called with exact number

> called with exact number

```javascript
SinonMockTest.prototype.testThatMyMethodGetCalledExactlySevenTimes = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	theSinonMock.expects("myMethod").exactly(7);
	sinonMock.callMyMethod(7);
	theSinonMock.verify();
};
```

It check if “myMethod” function get called exactly seven times or not.  

#### Test if my method get called between min to max times

> called between min to max

```javascript
SinonMockTest.prototype.testThatMyMethodGetCalledAtLeastTwiceAndAtMostFourTimes = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	theSinonMock.expects("myMethod").atLeast(2);
	theSinonMock.expects("myMethod").atMost(4);
	sinonMock.callMyMethod(4);
	theSinonMock.verify();
};
```

It check if “myMethod” function get called between a range of min to max time or not. therefore, for above example,any number of calls between 2 to 4 should be meet our expectation.
#### Test if my method get never called

> never called

```javascript
SinonMockTest.prototype.testThatMyMethodGetNeverCalled = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	theSinonMock.expects("myMethod").never();
	sinonMock.callMyMethod(0);
	theSinonMock.verify();
};
```

It check if “myMethod” function get never called or not. Therefore, if “myMethod” get called during our code execution then it will not meet our expectation and our test will be fail.

#### Test that my method get called with args

> with args

```javascript
SinonMockTest.prototype.testThatMyMethodGetCalledWithArgs = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	var firstArgument = 'first argument';
	var secondArgument = 'second argument';
	theSinonMock.expects("myMethod").withArgs(firstArgument);
	sinonMock.callMyMethod(1,firstArgument,secondArgument);
	theSinonMock.verify();
};
```

it check if “myMethod” get called with our expected args or not. it check if our expected argument existed in the passed argument or not.  

#### Test that my method get called with exact args

> called with exact args

```
SinonMockTest.prototype.testThatMyMethodGetCalledWithExactArgs = function () {
	var theSinonMock = this._sandbox.mock(sinonMock);
	var firstArgument = 'first argument';
	var secondArgument = 'second argument';
	theSinonMock.expects("myMethod").withExactArgs(firstArgument, secondArgument);
	sinonMock.callMyMethod(1,firstArgument,secondArgument);
	theSinonMock.verify();
};
```

It check if “myMethod” get call with all exact passed argument or not.

### Download

You can find the full API reference on [sinon mock documentation](http://sinonjs.org/docs/#mocks) . Also feel free to download the full source code of this example from my [github](https://github.com/inadram/Code-Sample/tree/master/javascript/sinon).

