---
 url : "optimise-images-in-seo"
 date : "2013-01-11"
 tags : 
  - "SEO"
 title : "Optimise images in SEO"
 description: "289776099636961280"
---

`“ALT”` attribute can help webmaster to provide image-related information for their pages (Google, Search Engine Optimization starter guide, 2010) (Google, Image publishing guidelines, 2011) (Linsley, 2009).
Google suggests webmasters to put related content around their images using brief but descriptive text in the “ALT” attribute of their images. This will help Googlebots more easily distinguish the topic of their pages (Falls, Goradia, & Perez, 2010).

{{<figure src="/media/14.jpg">}}

Figure 20: ALT attributes describe the image (Google, 2010)

The `“ALT”` attribute helps webmasters to specify alternative text for images. The alternative text can help their visitors to understand the images if they are not loading at the end. At the same time `“ALT”` attribute could act as an anchor text for an image link and help Googlebot to have more clues about the content of the target page.  

In addition it is quite useful to have a brief but descriptive file name for images rather than generalise them via filenames such as `“pic.gif”` or `“1.jpg”`.  
Grouping all of the similar sized pictures into one directory is recommended but avoid using extremely long names or keywords into ALT text and filenames (Google, Image publishing guidelines, 2011) (Linsley, 2009) (Google, Search Engine Optimization starter guide, 2010).

{{<figure src="/media/21.jpg">}}

Figure 21: Image folder on a website (Google, 2010)

Furthermore, Google asked webmaster to provide a standalone landing page with unique information for the images that would appear on different pages and gather all related information about it together (Google, Search Engine Optimization starter guide, 2010).  

Webmasters are recommended to specify the height and width of their images to help web browsers to render their pages more efficiently which can help them to improve the user experience (Google, Image publishing guidelines, 2011) (Linsley, 2009).  

The user would benefit from Google’s sitemap to submit additional information about the images on the website, this could also help Googlebot to discover the images which hidden under JavaScript forms (Google, Image Sitemaps, 2012).

### References
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Google. (2011). Image publishing guidelines. Retrieved January 7, 2012, from Webmaster Tools Help: [[link]](http://support.google.com/webmasters/bin/answer.py?hl=en&answer=114016)*
- *Linsley, P. (2009). Get up-to-date on Image Search. Retrieved January 7, 2012, from google webmaster central blog: [[link]](http://.blogspot.com/2009/03/get-up-to-date-on-image-search.html)*
- *Falls, B., Goradia, A., & Perez, C. (2010). Google’s SEO Report Card. Retrieved January 7, 2012, from Google Webmaster Central: [[link]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/google-seo-report-card.pdf)*
- *Google. (2012). Image Sitemaps. Retrieved January 7, 2012, from Google Webmaster Tools Help: [[link]](http://www.google.com/support/webmasters/bin/answer.py?answer=178636)*
