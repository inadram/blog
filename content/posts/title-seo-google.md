---
 url : "title-seo-google"
 date : "2012-12-08"
 tags : 
  - "SEO" 
 title : "what Google says about importance of Title in SEO"
 description: "278250791352008704"
---

`<Title>` tag should place in `<head>` section of the HTML document of a website and ideally each page should have its own unique title. Title usually is the first thing that appears in search result. (Google, Search Engine Optimization starter guide, 2010)

Home page title can be the name of the website and in addition it can contain the main activities of your website.

Page title is an important factor in Google ranking. According to its guidelines a good title should describe the content of the page accurately and also it ideally should be unique to help Google to distinguish a page from others on a website. According to the Google analysis about 28% of websites can improve their quality only by having unique title in each page (Lee & Douvas, 2010).

Google displays approximately first 60 characters of each title tag and Google may give less weight after that point (Falls, Goradia, & Perez, 2010), therefore a useful title should be brief and in the same point it should be descriptive to help both search engine and users have a clear understanding of the page subject.

Google suggests to webmasters to avoid lengthy titles, which have unneeded keywords, and usually do not use in search queries but recommend webmasters to put useful information such as main features on it as some users only look at titles.
A clever webmaster can benefit from its brand in the title as some users do not take a look at the URL of the result page (Falls, Goradia, & Perez, 2010) (Google, Search Engine Optimization starter guide, 2010).

### References
- *Falls, B., Goradia, A., & Perez, C. (2010). Google’s SEO Report Card. Retrieved January 7, 2012, from Google Webmaster Central: [[1]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/google-seo-report-card.pdf)*
- *Google. (2010, October 2). Search Engine Optimization starter guide. Retrieved January 2012, 7, from Google:[[2]](http://static.googleusercontent.com/external_content/untrusted_dlcp/www.google.com/en//webmasters/docs/search-engine-optimization-starter-guide.pdf)*
- *Google. (2010). Technology overview. Retrieved January 7, 2012, from Google: [[3]](http://www.google.com/about/corporate/company/tech.html)*
- *Lee, J., & Douvas, A. (2010). Ring in the new year with accessible content: Website clinic for non-profits. Retrieved January 7, 2012, from google webmaster central blog: [[4]](http://googlewebmastercentral.blogspot.com/2010/12/ring-in-new-year-with-accessible.html)*