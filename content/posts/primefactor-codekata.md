---
 url : "primefactor-codekata"
 date : "2012-08-19"
 tags : 
  - "primefactor" 
  - "CodeKata"
 title : "My Prime Factor Code Kata with Csharp"
 description : 237274981984264192
---

{{< youtube xhe3msQ1RME >}}  

---

The [prime factor][1] is an easy Code Kata but it might take a while to find the best solution to increase the code performance.
in re factoring part you will find out you do not need to go and iterate all the numbers to find the prime factors of the input number. in other words it is adequate to check the numbers which are less than the input number square root.

Download the code from my [Github][2]

[1]: http://mathworld.wolfram.com/PrimeFactor.html
[2]: https://github.com/inadram/CodeKata.git

