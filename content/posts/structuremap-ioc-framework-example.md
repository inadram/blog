---
 url : "structuremap-ioc-framework-example"
 date : "2013-02-19"
 tags : 
  - "Programming"
  - "IoC"
  - "Structuremap"
  - "C#"
 title : "Using Structuremap IoC framework example"
 description: "303996430845104128"
---

### What is Structuremap?
It is one of the oldest  IoC container frameworks which created by Jeremy Miller around June 2004. It is a stable open source framework that used in many projects. You can download it from Nuget  .

### What we have ?
We have a printer class that accepts printer type and do printing according to the passed type.

> print class

```
public class Print {
        private readonly ITypeOfPrinter _typeOfPrinter; 
        public Print(ITypeOfPrinter typeOfPrinter) {
            _typeOfPrinter = typeOfPrinter;
        } 
 
        public string print() {
          return _typeOfPrinter.Printing();
        }
}
```

The passed type can be Black and white or colorfull

> Type of printer

```
public class BlackAndWhitePrinter : ITypeOfPrinter {
        public string Printing() {
            NumberOfPrints++;
            return string.Format("it is Black and white printing {0}",NumberOfPrints); 
        }
 
        public int NumberOfPrints { get; set; }
}
```
    
### How to use StructureMap
The structuremap using lambda syntax and we can registering the types with For and Use keywords.

> structuremap container  

```
var container = new Container();
container.Configure(x=>x.For<ITypeOfPrinter>().Use<ColorfullPrinter>());
var printerInstance = container.GetInstance<Print>();
string colorfullPrintText = printerInstance.print();
```

In this example we ask the constructor of the Print class to use the colorful printer type when it’s looking for ITypeOfPrinter.

#### Using Structuremap with dependency

It is also possible to pass the dependency to the container directly rather than using the Configure method.

> structuremap with dependency

```
var container = new Container(x => x.For<ITypeOfPrinter>().Use<ColorfullPrinter>());
var printerInstance = container.GetInstance<Print>();
string colorfullPrintText = printerInstance.print();
```

#### Registering all the dependencies in one place
We can use Registry class in the Structure map. Configuration. DSL name space to registering the dependencies in a separate method.

In this case we should implement this class and passing the registration in its constructor.

> registering

```
public class Registering:Registry {
        public Registering() {
            For<ITypeOfPrinter>().Use<BlackAndWhitePrinter>();
        }
}
```

Then we can initialize our container easily by calling this method.

> Using structuremap with dependency

```
var container = new Container(new Registering());
var printerInstance = container.TryGetInstance<ITypeOfPrinter>();
string blackAndWhitePrintText = printerInstance.Printing();
```

#### Unity singleton Lifecycle
Similar to the other IoC framework by default every time we resolve a class it make a new instance of it. In order to treat it as a singleton and using the static properties we can use SingeltonLifecycle() method.

> singelton lifecycle

```
var container = new Container();
container.Configure(x=>x.For<ITypeOfPrinter>().LifecycleIs(new SingletonLifecycle()).Use<BlackAndWhitePrinter>());
var printerInstance = container.GetInstance<Print>();
var printerInstance2 = container.GetInstance<Print>(); 
string blackAndWhitePrintText = printerInstance.print();
string blackAndWhitePrintText2 = printerInstance2.print();
```

In this case the printer and print2 has a different number of prints values.

### Download
Feel free to download the full source code of this example from my [GitHub](https://github.com/inadram/Code-Sample/blob/master/IoC/IoC/IoC/TestStructuremap.cs).
