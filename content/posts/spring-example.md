---
 url : "spring-example"
 date : "2014-05-06"
 tags : 
  - "Programming"
  - "Java"
  - "Spring"
 title : "Spring example?"
 description: "463753882804305920"
---

We have a simple interface driven application below that we tries to move it to spring framework.

### Old Code
#### Customer DTO
Customer Data Transfer Object contains firstName and lastName variables and their setters and getters.

```java 
public class Customer {
 
    private String firstName;
    private String lastName;
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
```

#### Hibernate Repository
It is our hard coded repository that simply use our Customer DTO and return a list of customers.

```java 
public class hibernateRepositoryImplementation implements CustomerRepository {
 
    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<Customer>();
 
        Customer customer = new Customer();
        customer.setFirstName("Amir");
        customer.setLastName("Mardani");
        customers.add(customer);
        return customers;
    }
}
```
#### Interface Repository

```java
public interface CustomerRepository {
    List<Customer> findAll();
}
```
#### Customer Service
It use hibernate repository and returns list of customers.

```
public class CustomerServiceImplementation implements CustomerService {
    private CustomerRepository customerRepository = new hibernateRepositoryImplementation();
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
}
```
#### Customer Service Interface

```
public interface CustomerService {
    List<Customer> findAll();
}
```

#### Main Class
It use the customer service and print the first customer name.

> Main Class

```
public class Application {
    public static void main(String args[]){
        CustomerService customerService=new CustomerServiceImplementation();
        System.out.println(customerService.findAll().get(0).getFirstName());
    }
}
```

### Download Libraries
#### Download Spring Library
You need to download the spring framework from spring repository and add them as library to your project.

- Spring-beans-version.jar
- Spring-context-version.jar
- Spring-core-version.jar
- Spring-expression-version.jar  

#### Download Commons Logging Library
It help us to have standard logging system across libraries. You can download it from apache website and add it as a library to your project.

- commons-logging-version.jar  

### Using Spring
#### Creating XML configuration file
In Spring you can manage your configuration via xml files. You need to create a valid XML file and put it under your source folder.

> applicationContext.xml

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
 
    <bean name="customerRepo" class="com.inadram.repo.hibernateRepositoryImplementation"/>
    <bean name="customerService" class="com.inadram.service.CustomerServiceImplementation">
        <property name="customerRepository" ref="customerRepo"/>
    </bean>
</beans>
```

#### What does it contains?
it can have Id or Name to represent your config identity and className to map them to your concrete implementation.

#### How it is works?

for instance in this example we are mapping Hibernate Implementation as a setter injection to the Customer Service Implementation.

> hibernate implementation

```
<bean name="customerRepo" class="com.inadram.repo.hibernateRepositoryImplementation"/>
```
name: it can be any name.

class : class path of the concrete implementation.

> customer service implementation

```
<bean name="customerService" class="com.inadram.service.CustomerServiceImplementation">
 <property name="customerRepository" ref="customerRepo"/>
</bean>
```
property name: is our setter property name that we are going to inject into service class.

ref: it should be the name of concrete class that we defined earlier.

#### Update Customer Service Implementation
we need to define setter injection to pass the customer repository interface

> customer service

```java
public class CustomerServiceImplementation implements CustomerService {
    private CustomerRepository customerRepository;
 
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }
 
    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }
}
```

#### Using it
in our main class we need to read the xml configuration file and use the appropriate bean implementation to call our Customer Service.

> call service

```
ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
CustomerService customerService = applicationContext.getBean("customerService", CustomerService.class);
System.out.println(customerService.findAll().get(0).getFirstName());
```
    
Class path: xml configuration name if you put your config in the root.
getBeans: bean name that you had defined in the xml file and you wish your dependency using it.

### Download
Feel free to download the full source code of this example from my [github](https://github.com/inadram/Code-Sample/tree/master/Java/Spring/spring-example).
