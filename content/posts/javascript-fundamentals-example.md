---
 url : "javascript-fundamentals-example"
 date : "2013-03-23"
 tags : 
  - "Programming"
  - "JavaScript"
 title : "JavaScript fundamentals by example"
 description: "315524764414386176"
---

### What is JavaScript?
It is a scripting language that runs on the client side. JavaScript is the language of the browser which is  dynamic and have OO and functional features.

### IDE
There are a couple of good IDE that help you to develop JavaScript functions such as VS2012, WebStorm , DreamWeaver and Sublime. There are also some online editors that you can run your code and get instant feedback such as JsBin.com  and jsdb.org .

### Define Variable
We can define a variable by “var” keyword. The keyword and the type of it will be define when we are initializing it.


```
function add(first, second) {
    sum = first + second;
    return sum;
}
```
```
var someVar = add(10, 5);
alert(typeof someVar + ' ' + someVar); 
someVar = add("Test", "it");
alert(typeof someVar + ' ' + someVar);
```

In the above code, “someVar” can be integer or string according to the type of passing variables.

#### Global Variables
By default all variables are global and you can access to them even if they are defined inside a method only. For example, in the above example you can access to the “sum” variable.


```
alert(typeof sum + ' ' + sum);
if (window.sum) {
    alert("sum is global variable");
}
```

#### Undefined or Null values
The default value of  null or undefined variables are false.

```
var someNull = null;
if (!someNull) {
    alert("null default value is false");
} 

var someUnDefined;
if (!someUnDefined) {
    alert(someUnDefined);
    alert("UnDefined default value is false");
}
```

#### Object
Define object in JavaScript is easy .

```
var emptyObject = {};
alert(typeof emptyObject + emptyObject.toString());
```

Also it is possible to define a “var” directly then initialise the sub element of it.


```
var amir={};
amir.name = "Amir";
alert(amir.name);
```

An object can have more than 1 sub element and indent.


```
var amir =
    {
        fullname:
        {
            firstName: "amir",
            LastName: "mardani"
        },
        name: "my name is amir",
        age: 26
    }; 

alert(amir.name);
alert(amir.age);
alert(amir.fullname.LastName);
```

##### Objects are reference type
If you pass an object into a method it will pass a reference of it into it . Therefore if you change that value of it inside the method it will be update the value of that object in all over the script.

```
var amir = { name: "amir" };
function changeName(obj, newName) {
    obj.name = newName;
}

alert(amir.name);
changeName(amir, "Amir");
alert(amir.name);
```

#### Equality
Do not use a == operator to compare the values as it checks the reference rather than their values. Always use === to check the values.

```
alert("0==''" + (0 == ''));
alert("0===''" + (0 === ''));
```

The first equation is return true but the second one return false as the values are not equal.

#### Functions
Functions can define in JavaScript by name or anonymously. Name functions appear in the call stack and are easier to debug code with them in comparing with anonymous functions.

Name functions can be defined by these two below syntaxes:

```
var MultipleByName = function (x, y) {
    return x * y;
};

function Multiple(x, y) {
    return x * y;
}

alert(MultipleByName(10, 5));
alert(Multiple(12, 5));
```

Anonymous iffy functions can be defined as below:

```
(function (x, y) {
    alert(x * y);
})(10, 10);
```

##### No overload in javascript
We do not have overload in javascript and the latest signature will be override it .For example in below code

```
function Multiple(x, y, z) {
    return x * y * z;
}

function Multiple(x, y) {
    return x * y;
} 

alert(Multiple(12, 5));
alert(Multiple(12, 5, 10));
```

Both called will be alert “60” as the Multiple (x, y) come after the Multiple (x, y, z) and it is overridden it. Therefore the Multiple (12,5,10) will be returned 60 and the third parameter will be ignored.

### Download
Feel free to download the full source code of these examples from my [github](https://github.com/inadram/Code-Sample/blob/master/javascript/fundemental/introduction.js).

